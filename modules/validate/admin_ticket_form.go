package validate

import (
	"github.com/go-macaron/binding"
	"gopkg.in/macaron.v1"
)

type AdminCreateTicketForm struct {
	Name        string  `json:"name" binding:"Required;MaxSize(100)"`
	Description string  `json:"description" binding:"Required"`
	CommitHash  string  `json:"commit_hash"`
	CategoryUID int64   `json:"category_uid" binding:"Required"`
	ProjectUID  []int64 `json:"project_uids" binding:"Required"`
	Status      int     `json:"status" binding:"Required"`

	IsNotify bool `json:"is_notify"`
}

func (f *AdminCreateTicketForm) Validate(ctx *macaron.Context, errs binding.Errors) binding.Errors {
	return validate(errs, ctx.Data, f, ctx.Locale)
}

type AdminEditTicketForm struct {
	ID          int64   `json:"id"`
	Name        string  `json:"name" binding:"Required;MaxSize(100)"`
	Description string  `json:"description" binding:"Required"`
	CommitHash  string  `json:"commit_hash"`
	CategoryUID int64   `json:"category_uid" binding:"Required"`
	ProjectUID  []int64 `json:"project_UIDs" binding:"Required"`
	Status      int     `json:"status" binding:"Required"`

	IsNotify bool `json:"is_notify"`
}

func (f *AdminEditTicketForm) Validate(ctx *macaron.Context, errs binding.Errors) binding.Errors {
	return validate(errs, ctx.Data, f, ctx.Locale)
}
