package validate

import (
	"github.com/go-macaron/binding"
	"gopkg.in/macaron.v1"
)

type AdminCreateCountryForm struct {
	Name      string `json:"name" binding:"MaxSize(100)"`
	ShortName string `json:"shot_name" binding:"Required;AlphaDashDot;MaxSize(2)"`
	IsActive  bool   `json:"is_active"`
}

func (f *AdminCreateCountryForm) Validate(ctx *macaron.Context, errs binding.Errors) binding.Errors {
	return validate(errs, ctx.Data, f, ctx.Locale)
}

type AdminEditCountryForm struct {
	ID        int64  `json:"id"`
	Name      string `json:"name" binding:"MaxSize(100)"`
	ShortName string `json:"shot_name" binding:"Required;AlphaDashDot;MaxSize(2)"`
	IsActive  bool   `json:"is_active"`
}

func (f *AdminEditCountryForm) Validate(ctx *macaron.Context, errs binding.Errors) binding.Errors {
	return validate(errs, ctx.Data, f, ctx.Locale)
}
