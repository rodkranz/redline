package validate

import (
	"github.com/go-macaron/binding"
	"gopkg.in/macaron.v1"
)

type AdminCreateProjectForm struct {
	Name        string `json:"name" binding:"Required;MaxSize(35)"`
	Description string `json:"description" binding:"Required"`
	IsActive    bool   `json:"is_active"`
}

func (f *AdminCreateProjectForm) Validate(ctx *macaron.Context, errs binding.Errors) binding.Errors {
	return validate(errs, ctx.Data, f, ctx.Locale)
}

type AdminEditProjectForm struct {
	ID          int64  `json:"id"`
	Name        string `json:"name" binding:"Required;MaxSize(100)"`
	Description string `json:"description" binding:"Required"`
	IsActive    bool   `json:"is_active"`
}

func (f *AdminEditProjectForm) Validate(ctx *macaron.Context, errs binding.Errors) binding.Errors {
	return validate(errs, ctx.Data, f, ctx.Locale)
}
