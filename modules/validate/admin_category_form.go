package validate

import (
	"github.com/go-macaron/binding"
	"gopkg.in/macaron.v1"
)

type AdminCreateCategoryForm struct {
	Name        string `json:"name" binding:"Required;AlphaDashDot;MaxSize(150)"`
	Description string `json:"description" binding:"Required"`
	IsActive    bool   `json:"is_active"`
}

func (f *AdminCreateCategoryForm) Validate(ctx *macaron.Context, errs binding.Errors) binding.Errors {
	return validate(errs, ctx.Data, f, ctx.Locale)
}

type AdminEditCategoryForm struct {
	ID          int64  `json:"id"`
	Name        string `json:"name" binding:"Required;AlphaDashDot;MaxSize(150)"`
	Description string `json:"description" binding:"Required"`
	IsActive    bool   `json:"is_active"`
}

func (f *AdminEditCategoryForm) Validate(ctx *macaron.Context, errs binding.Errors) binding.Errors {
	return validate(errs, ctx.Data, f, ctx.Locale)
}
