package validate

import (
	"github.com/go-macaron/binding"
	"gopkg.in/macaron.v1"
)

type AdminCreateUserForm struct {
	ID          int64  `json:"id"`
	FullName    string `json:"full_name" binding:"MaxSize(100)"`
	Username    string `json:"username" binding:"Required;AlphaDashDot;MaxSize(35)"`
	Email       string `json:"email" binding:"Required;Email;MaxSize(254)"`
	Password    string `json:"password" binding:"MaxSize(255)"`
	Description string `json:"description" binding:"Text"`
	IsActive    bool   `json:"is_active"`
	IsAdmin     bool   `json:"is_admin"`
}

func (f *AdminCreateUserForm) Validate(ctx *macaron.Context, errs binding.Errors) binding.Errors {
	return validate(errs, ctx.Data, f, ctx.Locale)
}

type AdminEditUserForm struct {
	ID          int64  `json:"id"`
	FullName    string `json:"full_name" binding:"MaxSize(100)"`
	Username    string `json:"username" binding:"Required;AlphaDashDot;MaxSize(35)"`
	Email       string `json:"email" binding:"Required;Email;MaxSize(254)"`
	Password    string `json:"password" binding:"MaxSize(255)"`
	Description string `json:"description" binding:"Text"`
	IsActive    bool   `json:"is_active"`
	IsAdmin     bool   `json:"is_admin"`
}

func (f *AdminEditUserForm) Validate(ctx *macaron.Context, errs binding.Errors) binding.Errors {
	return validate(errs, ctx.Data, f, ctx.Locale)
}

type AdminUserAssociateProjectForm struct {
	ProjectUID int64 `json:"project_uid"`
}

func (f *AdminUserAssociateProjectForm) Validate(ctx *macaron.Context, errs binding.Errors) binding.Errors {
	return validate(errs, ctx.Data, f, ctx.Locale)
}
