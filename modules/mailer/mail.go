package mailer

import (
	"fmt"

	"gopkg.in/gomail.v2"
	"gopkg.in/macaron.v1"

	"github.com/rlopes-fixeads/redLine/models"
	"github.com/rlopes-fixeads/redLine/modules/base"
	"github.com/rlopes-fixeads/redLine/modules/log"
	"github.com/rlopes-fixeads/redLine/modules/setting"
)

const (
	AUTH_ACTIVATE        base.TplName = "mail/auth/activate"
	AUTH_ACTIVATE_EMAIL  base.TplName = "mail/auth/activate_email"
	AUTH_REGISTER_NOTIFY base.TplName = "mail/auth/register_notify"
	AUTH_RESET_PASSWORD  base.TplName = "mail/auth/reset_passwd"

	NOTIFY_COLLABORATOR base.TplName = "mail/notify/collaborator"
	NOTIFY_MENTION      base.TplName = "mail/notify/mention"
)

func ComposeTplData(u *models.User) map[interface{}]interface{} {
	data := make(map[interface{}]interface{}, 10)
	data["AppName"] = setting.AppName
	data["AppVer"] = setting.AppVer
	data["AppUrl"] = setting.AppUrl
	data["ActiveCodeLives"] = setting.Service.ActiveCodeLives / 60
	data["ResetPwdCodeLives"] = setting.Service.ResetPwdCodeLives / 60

	if u != nil {
		data["User"] = u
	}
	return data
}

func SendUserMail(c *macaron.Context, u *models.User, tpl base.TplName, code, subject, info string) {
	data := ComposeTplData(u)
	data["Code"] = code
	body, err := c.HTMLString(string(tpl), data)
	if err != nil {
		log.Error(4, "HTMLString: %v", err)
		return
	}

	msg := NewMessage([]string{u.Email}, subject, body)
	msg.Info = fmt.Sprintf("UID: %d, %s", u.ID, info)

	SendAsync(msg)
}

func SendActivateAccountMail(c *macaron.Context, u *models.User) {
	SendUserMail(c, u, AUTH_ACTIVATE, u.GenerateActivateCode(), c.Tr("mail.activate_account"), "activate account")
}

// SendResetPasswordMail sends reset password e-mail.
func SendResetPasswordMail(c *macaron.Context, u *models.User) {
	SendUserMail(c, u, AUTH_RESET_PASSWORD, u.GenerateActivateCode(), c.Tr("mail.reset_password"), "reset password")
}

// SendRegisterNotifyMail triggers a notify e-mail by admin created a account.
func SendRegisterNotifyMail(c *macaron.Context, u *models.User) {
	body, err := c.HTMLString(string(AUTH_REGISTER_NOTIFY), ComposeTplData(u))
	if err != nil {
		log.Error(4, "HTMLString: %v", err)
		return
	}

	msg := NewMessage([]string{u.Email}, c.Tr("mail.register_notify"), body)
	msg.Info = fmt.Sprintf("UID: %d, registration notify", u.ID)

	SendAsync(msg)
}

// SendActivateAccountMail sends confirmation e-mail.
func SendActivateEmailMail(c *macaron.Context, u *models.User, email *models.EmailAddress) {
	data := ComposeTplData(u)
	data["Code"] = u.GenerateEmailActivateCode(email.Email)
	data["Email"] = email.Email
	body, err := c.HTMLString(string(AUTH_ACTIVATE_EMAIL), data)
	if err != nil {
		log.Error(4, "HTMLString: %v", err)
		return
	}

	msg := NewMessage([]string{email.Email}, c.Tr("mail.activate_email"), body)
	msg.Info = fmt.Sprintf("UID: %d, activate email", u.ID)

	SendAsync(msg)
}

func SendTestMail(email string) error {
	return gomail.Send(&Sender{}, NewMessage([]string{email}, "RedLine Test Email!", "RedLine Test Email!").Message)
}
