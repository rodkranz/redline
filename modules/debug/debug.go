package debug

import (
	"reflect"

	"github.com/rlopes-fixeads/redLine/modules/log"
)

func Details(s interface{}) {

	t := reflect.TypeOf(s)
	r := reflect.ValueOf(s)

	log.Debug("Type: %v - %v", t, r)

}
