package routers

import (
	"strings"

	"gopkg.in/macaron.v1"

	"github.com/rlopes-fixeads/redLine/models"
	"github.com/rlopes-fixeads/redLine/modules/log"
	"github.com/rlopes-fixeads/redLine/modules/markdown"
	"github.com/rlopes-fixeads/redLine/modules/setting"
)

func NewServices() {
	setting.NewServices()
}

func GlobalInit() {
	setting.NewContext()

	log.Trace("Custom path: %s", setting.CustomPath)
	log.Trace("Log path: %s", setting.LogRootPath)

	models.LoadConfigs()
	NewServices()

	if err := models.NewEngine(); err != nil {
		log.Fatal(4, "Fail to initialize ORM engine: %v", err)
	}
	models.HasEngine = true

	// Build Sanitizer
	markdown.BuildSanitizer()

	checkRunMode()
}

func checkRunMode() {
	switch setting.Cfg.Section("").Key("RUN_MODE").String() {
	case "prod":
		macaron.Env = macaron.PROD
		macaron.ColorLog = false
		setting.ProdMode = true
	}
	log.Info("Run Mode: %s", strings.Title(macaron.Env))
}
