package user

import (
	"github.com/rlopes-fixeads/redLine/models"
	"github.com/rlopes-fixeads/redLine/modules/base"
	"github.com/rlopes-fixeads/redLine/modules/context"
)

const (
	DASHBOARD base.TplName = "user/dashboard/dashboard"
	PROFILE   base.TplName = "user/profile"
)

func getDashboardContextUser(ctx *context.Context) *models.User {
	ctxUser := ctx.User
	ctx.Data["ContextUser"] = ctxUser
	return ctxUser
}

func Dashboard(ctx *context.Context) {
	ctxUser := getDashboardContextUser(ctx)
	ctx.Data["Title"] = ctxUser.DisplayName() + " - " + ctx.Tr("dashboard")
	ctx.Data["PageIsDashboard"] = true
	ctx.Data["PageIsNews"] = true

	if ctx.Written() {
		return
	}

	ctx.HTML(200, DASHBOARD)
}
