package user

import (
	"github.com/rlopes-fixeads/redLine/models"
	"github.com/rlopes-fixeads/redLine/modules/context"
)

func prepareCategories(ctx *context.Context) {
	var err error
	ctx.Data["Categories"], err = models.AllCategories()
	if err != nil {
		ctx.Data["Categories"] = make([]models.Category, 0)
	}
}
