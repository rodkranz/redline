package user

import (
	"strings"

	"github.com/rlopes-fixeads/redLine/models"
	"github.com/rlopes-fixeads/redLine/modules/base"
	"github.com/rlopes-fixeads/redLine/modules/context"
	"github.com/rlopes-fixeads/redLine/modules/log"
	"github.com/rlopes-fixeads/redLine/modules/setting"
	"github.com/rlopes-fixeads/redLine/modules/validate"
)

const (
	SETTINGS_PROFILE  base.TplName = "user/settings/profile"
	SETTINGS_PASSWORD base.TplName = "user/settings/password"
)

func Settings(ctx *context.Context) {
	ctx.Data["Title"] = ctx.Tr("settings")
	ctx.Data["PageIsSettingsProfile"] = true
	ctx.HTML(200, SETTINGS_PROFILE)
}

func handleUsernameChange(ctx *context.Context, newName string) {
	// Non-local users are not allowed to change their username.
	if len(newName) == 0 {
		return
	}

	// Check if user name has been changed
	if ctx.User.Username != strings.ToLower(newName) {
		if err := models.ChangeUserName(ctx.User, newName); err != nil {
			switch {
			case models.IsErrUserAlreadyExist(err):
				ctx.Flash.Error(ctx.Tr("form.newName_been_taken"))
				ctx.Redirect(setting.AppSubUrl + "/user/settings")
			case models.IsErrEmailAlreadyUsed(err):
				ctx.Flash.Error(ctx.Tr("form.email_been_used"))
				ctx.Redirect(setting.AppSubUrl + "/user/settings")
			default:
				ctx.Handle(500, "ChangeUserName", err)
			}

			return
		}
		log.Trace("User name changed: %s -> %s", ctx.User.Username, newName)
	}

	// In case it's just a case change
	ctx.User.Username = strings.ToLower(newName)
}

func SettingsPost(ctx *context.Context, form validate.UpdateProfileForm) {
	ctx.Data["Title"] = ctx.Tr("settings")
	ctx.Data["PageIsSettingsProfile"] = true

	if ctx.HasError() {
		ctx.HTML(200, SETTINGS_PROFILE)
		return
	}

	handleUsernameChange(ctx, form.UserName)
	if ctx.Written() {
		return
	}

	ctx.User.FullName = form.FullName
	ctx.User.Email = form.Email
	ctx.User.Username = form.UserName

	if err := models.UpdateUser(ctx.User); err != nil {
		ctx.Handle(500, "UpdateUser", err)
		return
	}

	log.Trace("User settings updated: %s", ctx.User.Username)
	ctx.Flash.Success(ctx.Tr("settings.profile.update_profile_success"))
	ctx.Redirect(setting.AppSubUrl + "/user/settings")
}

func SettingsPassword(ctx *context.Context) {
	ctx.Data["Title"] = ctx.Tr("settings")
	ctx.Data["PageIsSettingsPassword"] = true

	ctx.HTML(200, SETTINGS_PASSWORD)
}

func SettingsPasswordPost(ctx *context.Context, form validate.ChangePasswordForm) {
	ctx.Data["Title"] = ctx.Tr("settings")
	ctx.Data["PageIsSettingsPassword"] = true

	if ctx.HasError() {
		ctx.HTML(200, SETTINGS_PASSWORD)

		return
	}

	if !ctx.User.ValidatePassword(form.OldPassword) {
		ctx.Flash.Error(ctx.Tr("settings.profile.password_incorrect"))
	} else if form.Password != form.Retype {
		ctx.Flash.Error(ctx.Tr("form.password_not_match"))
	} else {
		ctx.User.Passwd = form.Password
		ctx.User.Salt = models.GetUserSalt()
		ctx.User.EncodePasswd()

		if err := models.UpdateUser(ctx.User); err != nil {
			ctx.Handle(500, "UpdateUser", err)

			return
		}

		log.Trace("User password updated: %s", ctx.User.Username)
		ctx.Flash.Success(ctx.Tr("settings.profile.change_password_success"))
	}

	ctx.Redirect(setting.AppSubUrl + "/user/settings/password")
}
