package user

import (
	"github.com/rlopes-fixeads/redLine/models"
	"github.com/rlopes-fixeads/redLine/modules/context"
)

/**
 * Projects that user has access
 */
func prepareProjectsFromUser(ctx *context.Context) {
	_, p, err := models.GetProjectsWithAssociationUser(ctx.User.ID)
	if err != nil {
		if !models.IsErrUserNotExist(err) {
			ctx.Handle(500, "GetUserByID", err)
		}
		return
	}
	ctx.Data["UserProjects"] = p
}

func prepareProjectsUserNotAssociated(ctx *context.Context) {
	_, p, err := models.GetProjectsWithoutAssociationUser(ctx.User.ID)
	if err != nil {
		if !models.IsErrUserNotExist(err) {
			ctx.Handle(500, "GetUserByID", err)
		}
		return
	}
	ctx.Data["UnassociatedProjects"] = p
}

func prepareProjects(ctx *context.Context) {
	var err error
	ctx.Data["Projects"], err = models.GetAllProjects()
	if err != nil {
		ctx.Data["Projects"] = make([]models.Project, 0)
	}
}
