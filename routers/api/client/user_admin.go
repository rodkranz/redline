package client

type CreateUserOption struct {
	Id         int64  `json:"id"`
	FullName   string `json:"fullname"`
	Username   string `json:"username" binding:"Required;AlphaDashDot;MaxSize(35)"`
	Email      string `json:"email" binding:"Required;Email;MaxSize(254)"`
	Password   string `json:"password" binding:"MaxSize(255)"`
	SendNotify bool   `json:"send_notify"`
	IsAdmin    bool   `json:"is_admin"`
}

type EditUserOption struct {
	Id          int64  `json:"id"`
	FullName    string `json:"fullname" binding:"MaxSize(100)"`
	Username    string `json:"Username" binding:"MaxSize(100)"`
	Email       string `json:"email" binding:"Required;Email;MaxSize(254)"`
	Password    string `json:"password" binding:"MaxSize(255)"`
	Description string `json:"website"  binding:"MaxSize(255)"`
	Active      *bool  `json:"active"`
	Admin       *bool  `json:"admin"`
}
