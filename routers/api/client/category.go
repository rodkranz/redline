package client

import "time"

type Category struct {
	ID          int64     `json:"id"`
	Name        string    `json:"name"`
	Description string    `json:"description"`
	IsActive    bool      `json:"is_active"`
	Created     time.Time `json:"created_at"`
	CreatedUnix int64
	Updated     time.Time `json:"updated_at"`
	UpdatedUnix int64
}
