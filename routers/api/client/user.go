package client

type User struct {
	ID       int64  `json:"id"`
	UserName string `json:"username"`
	FullName string `json:"full_name"`
	Email    string `json:"email"`
}

type UserLogin struct {
	ID       int64  `json:"id"`
	UserName string `json:"username"`
	FullName string `json:"full_name"`
	Email    string `json:"email"`
	Token    string `json:"token"`
}

type SignUpUserOptions struct {
	FullName string `json:"full_name" binding:"Required;AlphaDashDot;MaxSize(35)"`
	UserName string `json:"username" binding:"Required;AlphaDashDot;MaxSize(35)"`
	Email    string `json:"email" binding:"Required;Email;MaxSize(254)"`
	Password string `json:"password" binding:"MaxSize(255)"`
	Retry    string `json:"retry" binding:"MaxSize(255)"`
}
