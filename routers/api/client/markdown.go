package client

type MarkdownOption struct {
	Text    string
	Mode    string
	Context string
}
