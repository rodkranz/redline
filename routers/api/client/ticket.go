package client

import "time"

type Ticket struct {
	ID          int64  `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	CommitHash  string `json:"commit_hash"`

	Category *Category `json:"category"`

	IsNotify bool `json:"is_notify"`

	Status int `json:"status"`

	Projects []*Project `json:"projects"`

	CreatedBy   int64     `json:"created_by"`
	Created     time.Time `json:"created_at"`
	CreatedUnix int64

	UpdatedBy   int64     `json:"updated_by"`
	Updated     time.Time `json:"updated_at"`
	UpdatedUnix int64
}
