package v1

import (
	"github.com/go-macaron/binding"
	"gopkg.in/macaron.v1"

	"github.com/rlopes-fixeads/redLine/modules/context"
	"github.com/rlopes-fixeads/redLine/modules/validate"
	"github.com/rlopes-fixeads/redLine/routers/api/client"
	"github.com/rlopes-fixeads/redLine/routers/api/v1/admin"
	"github.com/rlopes-fixeads/redLine/routers/api/v1/common"
	"github.com/rlopes-fixeads/redLine/routers/api/v1/misc"
	"github.com/rlopes-fixeads/redLine/routers/api/v1/user"

	api "github.com/rlopes-fixeads/redLine/routers/api/client"
)

// Contexter middleware already checks token for user sign in process.
func ReqToken() macaron.Handler {
	return func(ctx *context.APIContext) {
		if !ctx.IsSigned {
			ctx.Error(401, "Unauthorized access", nil)
			return
		}
	}
}

func ReqAdmin() macaron.Handler {
	return func(ctx *context.APIContext) {
		if !ctx.User.IsAdmin {
			ctx.Error(403, "Forbidden access", nil)
			return
		}
	}
}

func RegisterRoutes(m *macaron.Macaron) {
	bind := binding.Bind

	//Api v1
	m.Group("/v1", func() {

		m.Post("/markdown", bind(api.MarkdownOption{}), misc.Markdown)
		m.Post("/markdown/raw", misc.MarkdownRaw)

		// Users without token
		m.Group("/users", func() {
			m.Get("/search", user.Search)

			m.Post("/sign_up", bind(validate.RegisterForm{}), user.SignUpPost)
			m.Post("/sign_in", bind(validate.SignInForm{}), user.SignInPost)

			m.Group("/:username", func() {
				m.Get("", user.GetUserInfo)
			})
		})

		m.Group("/users", func() {
			m.Get("/current", user.CurrentUser)
		}, ReqToken())

		m.Group("/tickets", func() {
			m.Combo("/:ticketid/info").Get(user.GetInfoTicketById)
		})

		// Four oh four page
		m.Any("/*", common.FourOhFour)

		m.Group("/admin", func() {
			m.Group("/users", func() {

				m.Combo("").Get(admin.GetUsers).Post(bind(client.CreateUserOption{}), admin.CreateUser)
				m.Combo("/:username").Get(admin.GetUser).Put(bind(client.EditUserOption{}), admin.EditUser).Delete(admin.DeleteUser)

			})
		}, ReqToken(), ReqAdmin())

	}, context.APIContexter())

}
