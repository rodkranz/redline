package user

import (
	"fmt"

	"github.com/rlopes-fixeads/redLine/models"
	"github.com/rlopes-fixeads/redLine/modules/context"
	"github.com/rlopes-fixeads/redLine/routers/api/v1/convert"
)

func GetInfoTicketById(ctx *context.APIContext) {
	t, err := models.GetTicketByID(ctx.ParamsInt64(":ticketid"))
	if err != nil {
		if models.IsErrTicketNotExist(err) {
			ctx.Error(404, "Ticket not found", nil)
			return
		}

		ctx.Error(500, "GetTicketByID", err)
		return
	}

	var userId int64 = 0
	if !ctx.User.IsAdmin {
		userId = ctx.User.ID
	}

	ctx.Render(200, fmt.Sprintf("Ticket found [%v]", t.ID), convert.ToTicket(t, userId))
}
