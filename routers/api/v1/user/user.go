package user

import (
	"github.com/Unknwon/com"

	"github.com/rlopes-fixeads/redLine/models"
	"github.com/rlopes-fixeads/redLine/modules/context"
	"github.com/rlopes-fixeads/redLine/routers/api/v1/convert"

	api "github.com/rlopes-fixeads/redLine/routers/api/client"
)

func Search(ctx *context.APIContext) {
	opts := &models.SearchUserOptions{
		Keyword:  ctx.Query("q"),
		Type:     models.USER_TYPE_INDIVIDUAL,
		PageSize: com.StrTo(ctx.Query("limit")).MustInt(),
	}

	if opts.PageSize == 0 {
		opts.PageSize = 10
	}

	users, _, err := models.SearchUserByName(opts)
	if err != nil {
		ctx.Error(500, "Erro to find users", err)
		return
	}

	results := make([]*api.User, len(users))
	for i := range users {
		results[i] = &api.User{
			ID:       users[i].ID,
			UserName: users[i].Username,
			FullName: users[i].FullName,
		}

		if ctx.IsSigned {
			results[i].Email = users[i].Email
		}
	}

	ctx.JSON(200, map[string]interface{}{
		"message":  "List of users found",
		"status":   200,
		"total":    len(users),
		"resource": results,
	})

}

func GetUserInfo(ctx *context.APIContext) {
	u, err := models.GetUserByName(ctx.Params(":username"))
	if err != nil {
		if models.IsErrUserNotExist(err) {
			ctx.Error(404, "User not found", nil)
			return
		}

		ctx.Error(500, "GetUserByName", err)
		return
	}

	if !ctx.IsSigned {
		u.Email = ""
	}

	ctx.Render(200, "User found", convert.ToUser(u))
}

func CurrentUser(ctx *context.APIContext) {
	u := ctx.User

	tokens, err := models.ListAccessTokens(u.ID)
	if err != nil {
		ctx.Error(500, "GetUserByName", err)
		return
	}

	if len(tokens) > 0 {
		ctx.Render(200, "Current user", convert.ToUserAuth(u, tokens[0]))
		return
	}

	ctx.Render(200, "Current user", convert.ToUser(u))
}
