package user

import (
	"fmt"

	"github.com/rlopes-fixeads/redLine/models"
	"github.com/rlopes-fixeads/redLine/modules/context"
	"github.com/rlopes-fixeads/redLine/modules/log"
	"github.com/rlopes-fixeads/redLine/modules/setting"
	"github.com/rlopes-fixeads/redLine/modules/validate"
	"github.com/rlopes-fixeads/redLine/routers/api/v1/convert"
)

func SignUpPost(ctx *context.APIContext, form validate.RegisterForm) {
	if form.Password != form.Retype {
		ctx.Error(400, "The user has been not registred successfully",
			map[string]interface{}{
				"password": "password_doesnt_match",
			})
		return
	}

	u := &models.User{
		FullName: form.FullName,
		Username: form.UserName,
		Email:    form.Email,
		Passwd:   form.Password,
		IsActive: !setting.Service.RegisterEmailConfirm,
		IsAdmin:  false,
	}

	var err error
	if err = models.CreateUser(u); err != nil {
		fmt.Printf("\n-%v\n", err.Error())
		switch {
		case models.IsErrUserAlreadyExist(err):
			ctx.Error(400, "form.username_been_taken", err)
		case models.IsErrEmailAlreadyUsed(err):
			ctx.Error(400, "form.email_been_used", err)
		case models.IsErrNameReserved(err):
			ctx.Error(400, "user.form.name_reserved", err)
		case models.IsErrNamePatternNotAllowed(err):
			ctx.Error(400, "user.form.name_pattern_not_allowed", err)
		default:
			ctx.Handle(500, "CreateUser", err)
		}
		return
	}
	log.Trace("Account created: %s", u.Username)

	ctx.Render(201, "The user has been registred successfully", convert.ToUser(u))
}

func SignInPost(ctx *context.APIContext, form validate.SignInForm) {

	u, t, err := models.UserSignIn(form.UserName, form.Password)
	if err != nil {
		if models.IsErrUserNotExist(err) || models.IsErrPasswordInvalid(err) {
			ctx.Error(400, "form.username_password_incorrect", err)
		} else {
			ctx.Handle(500, "UserSignIn", err)
		}
		return
	}

	ctx.Render(200, "The user has been logged successfully", convert.ToUserAuth(u, t))
}
