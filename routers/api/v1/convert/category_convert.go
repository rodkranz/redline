package convert

import (
	"github.com/rlopes-fixeads/redLine/models"
	api "github.com/rlopes-fixeads/redLine/routers/api/client"
)

func ToCategory(category *models.Category) *api.Category {
	if category == nil {
		return nil
	}

	return &api.Category{
		ID:          category.ID,
		Name:        category.Name,
		Description: category.Description,
		IsActive:    category.IsActive,
		Created:     category.Created,
		CreatedUnix: category.CreatedUnix,
		Updated:     category.Updated,
		UpdatedUnix: category.UpdatedUnix,
	}
}

func ToCategories(categories []*models.Category) []*api.Category {
	list := make([]*api.Category, len(categories))

	for i, c := range categories {
		list[i] = ToCategory(c)
	}

	return list
}
