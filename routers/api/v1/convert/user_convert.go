package convert

import (
	api "github.com/rlopes-fixeads/redLine/routers/api/client"

	"github.com/rlopes-fixeads/redLine/models"
)

func ToUser(u *models.User) *api.User {
	if u == nil {
		return nil
	}

	return &api.User{
		ID:       u.ID,
		UserName: u.Username,
		FullName: u.FullName,
		Email:    u.Email,
	}
}

func ToUsers(users []*models.User) []*api.User {
	list := make([]*api.User, len(users))

	for i, u := range users {
		list[i] = ToUser(u)
	}

	return list
}

func ToUserAuth(u *models.User, t *models.AccessToken) *api.UserLogin {
	if u == nil {
		return nil
	}

	return &api.UserLogin{
		ID:       u.ID,
		UserName: u.Username,
		FullName: u.FullName,
		Email:    u.Email,
		Token:    t.Sha1,
	}
}
