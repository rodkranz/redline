package convert

import (
	"github.com/rlopes-fixeads/redLine/models"
	api "github.com/rlopes-fixeads/redLine/routers/api/client"
)

func ToProject(project *models.Project) *api.Project {
	if project == nil {
		return nil
	}

	return &api.Project{
		ID:          project.ID,
		Name:        project.Name,
		Description: project.Description,
		IsActive:    project.IsActive,
		Status:      project.Status,
		Created:     project.Created,
		CreatedUnix: project.CreatedUnix,
		Updated:     project.Updated,
		UpdatedUnix: project.UpdatedUnix,
	}
}

func ToProjects(projects []*models.Project) []*api.Project {
	list := make([]*api.Project, len(projects))

	for i, c := range projects {
		list[i] = ToProject(c)
	}

	return list
}
