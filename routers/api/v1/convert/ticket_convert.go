package convert

import (
	"github.com/rlopes-fixeads/redLine/models"
	api "github.com/rlopes-fixeads/redLine/routers/api/client"
)

func ToTicket(ticket *models.Ticket, userId int64) *api.Ticket {
	if ticket == nil {
		return nil
	}

	projects := make([]*models.Project, 0)
	if userId == 0 {
		projects = ticket.GetProjects()
	} else {
		projects = ticket.GetProjectsForUser(userId)
	}

	return &api.Ticket{
		ID:          ticket.ID,
		Name:        ticket.Name,
		Description: ticket.Description,

		CommitHash: ticket.CommitHash,
		Category:   ToCategory(ticket.GetCategory()),
		Projects:   ToProjects(projects),
		IsNotify:   ticket.IsNotify,
		Status:     ticket.Status,

		Created:     ticket.Created,
		CreatedUnix: ticket.CreatedUnix,
		Updated:     ticket.Updated,
		UpdatedUnix: ticket.UpdatedUnix,
	}
}

func ToTickets(tickets []*models.Ticket) []*api.Ticket {
	list := make([]*api.Ticket, len(tickets))

	for i, c := range tickets {
		list[i] = ToTicket(c, 0)
	}

	return list
}
