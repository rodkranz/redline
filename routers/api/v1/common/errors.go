package common

import (
	"github.com/rlopes-fixeads/redLine/modules/context"
)

func FourOhFour(ctx *context.APIContext) {
	ctx.JSON(404, map[string]interface{}{
		"message":  "Page Not Found",
		"status":   404,
		"resource": nil,
	})
}
