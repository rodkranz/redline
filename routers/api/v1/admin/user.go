package admin

import (
	"github.com/rlopes-fixeads/redLine/models"
	"github.com/rlopes-fixeads/redLine/modules/context"
	"github.com/rlopes-fixeads/redLine/modules/log"
	"github.com/rlopes-fixeads/redLine/routers/api/client"
	"github.com/rlopes-fixeads/redLine/routers/api/v1/convert"
	"github.com/rlopes-fixeads/redLine/routers/api/v1/user"
)

func parseLoginSource(ctx *context.APIContext, u *models.User, sourceID int64, Username string) {
	if sourceID == 0 {
		return
	}

	source, err := models.GetUserByID(sourceID)
	if err != nil {
		if models.IsErrAuthenticationNotExist(err) {
			ctx.Error(422, "", err)
		} else {
			ctx.Error(500, "GetLoginSourceByID", err)
		}
		return
	}

	u.ID = source.ID
}

func GetUsers(ctx *context.APIContext) {
	users, err := models.Users(1, 10)
	if err != nil {
		ctx.Error(500, "Get all users", err)
		return
	}

	ctx.JSON(200, map[string]interface{}{
		"message":  "list of users",
		"status":   200,
		"total":    models.CountUsers(),
		"resource": convert.ToUsers(users),
	})
}

func GetUser(ctx *context.APIContext) {
	u := user.GetUserByParams(ctx)
	if ctx.Written() {
		return
	}

	ctx.Render(200, "User found", convert.ToUser(u))
}

func CreateUser(ctx *context.APIContext, form client.CreateUserOption) {
	u := &models.User{
		FullName: form.FullName,
		Username: form.Username,
		Email:    form.Email,
		Passwd:   form.Password,
		IsActive: true,
		IsAdmin:  form.IsAdmin,
	}

	if err := models.CreateUser(u); err != nil {
		if models.IsErrUserAlreadyExist(err) ||
			models.IsErrEmailAlreadyUsed(err) ||
			models.IsErrNameReserved(err) ||
			models.IsErrNamePatternNotAllowed(err) {
			ctx.Error(422, err.Error(), nil)
		} else {
			ctx.Error(500, "CreateUser", err)
		}
		return
	}
	log.Trace("Account created by admin (%s): %s", ctx.User.Username, u.Username)

	/*
		// Send e-mail notification.
		if form.SendNotify && setting.MailService != nil {
			mailer.SendRegisterNotifyMail(ctx.Context.Context, u)
		}*/

	ctx.Render(201, "User has been created successfully", convert.ToUser(u))
}

func EditUser(ctx *context.APIContext, form client.EditUserOption) {
	u := user.GetUserByParams(ctx)
	if ctx.Written() {
		return
	}

	parseLoginSource(ctx, u, form.Id, form.Username)
	if ctx.Written() {
		return
	}

	if len(form.Password) > 0 {
		u.Passwd = form.Password
		u.Salt = models.GetUserSalt()
		u.EncodePasswd()
	}

	u.Username = form.Username
	u.FullName = form.FullName
	u.Email = form.Email
	u.Description = form.Description

	if form.Active != nil {
		u.IsActive = *form.Active
	}

	if form.Admin != nil {
		u.IsAdmin = *form.Admin
	}

	if err := models.UpdateUser(u); err != nil {
		if models.IsErrEmailAlreadyUsed(err) {
			ctx.Error(422, "", err)
		} else {
			ctx.Error(500, "UpdateUser", err)
		}
		return
	}
	log.Trace("Account profile updated by admin (%s): %s", ctx.User.Username, u.Username)

	ctx.Render(200, "User has been updated successfully", convert.ToUser(u))
}

func DeleteUser(ctx *context.APIContext) {
	u := user.GetUserByParams(ctx)
	if ctx.Written() {
		return
	}

	if err := models.DeleteUser(u); err != nil {
		if models.IsErrUserNotExist(err) {
			ctx.Error(422, "", err)
			return
		}

		ctx.Error(500, "DeleteUser", err)
		return
	}
	log.Trace("Account deleted by admin(%s): %s", ctx.User.Username, u.Username)

	ctx.Status(204)
}
