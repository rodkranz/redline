package misc

import (
	"github.com/rlopes-fixeads/redLine/modules/context"
	"github.com/rlopes-fixeads/redLine/modules/markdown"

	api "github.com/rlopes-fixeads/redLine/routers/api/client"
)

func Markdown(ctx *context.APIContext, form api.MarkdownOption) {
	if ctx.HasApiError() {
		ctx.Error(422, "", ctx.GetErrMsg())
		return
	}

	if len(form.Text) == 0 {
		ctx.Write([]byte(""))
		return
	}

	switch form.Mode {
	case "gfm":
		ctx.Write(markdown.Render([]byte(form.Text), form.Context, nil))
	default:
		ctx.Write(markdown.RenderRaw([]byte(form.Text), ""))
	}
}

func MarkdownRaw(ctx *context.APIContext) {
	body, err := ctx.Req.Body().Bytes()
	if err != nil {
		ctx.Error(422, "", err)
		return
	}
	ctx.Write(markdown.RenderRaw(body, ""))
}
