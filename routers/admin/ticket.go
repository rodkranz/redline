package admin

import (
	/*"github.com/Unknwon/com"*/
	"github.com/Unknwon/paginater"

	"github.com/rlopes-fixeads/redLine/models"
	"github.com/rlopes-fixeads/redLine/modules/base"
	"github.com/rlopes-fixeads/redLine/modules/context"
	"github.com/rlopes-fixeads/redLine/modules/log"
	"github.com/rlopes-fixeads/redLine/modules/setting"
	"github.com/rlopes-fixeads/redLine/modules/validate"
)

const (
	TICKET_LIST base.TplName = "admin/ticket/explorer"
	TICKET_EDIT base.TplName = "admin/ticket/edit"
	TICKET_NEW  base.TplName = "admin/ticket/new"
	TICKET_VIEW base.TplName = "admin/ticket/view"
)

type TicketSearchOptions struct {
	Counter  func() int64
	Ranger   func(int, int) ([]*models.Ticket, error)
	OrderBy  string
	OwnerId  int64
	Status   int
	TplName  base.TplName
	Page     int
	PageSize int
}

func ViewTicket(ctx *context.Context) {
	ctx.Data["Title"] = ctx.Tr("admin.ticket.view_ticket")
	ctx.Data["PageIsUserTicket"] = true

	prepareTicketStatus(ctx)
	prepareCategories(ctx)
	prepareProjects(ctx)

	_, err := prepareTicketInfo(ctx)
	if err != nil && models.IsErrTicketNotExist(err) {
		ctx.Flash.Success(ctx.Tr("admin.tickets.not_found"))
		ctx.Redirect(setting.AppSubUrl + "/admin/tickets/")
		return
	}

	if ctx.Written() {
		return
	}

	ctx.HTML(200, TICKET_VIEW)
}

func RenderTicketSearch(ctx *context.Context, opts *TicketSearchOptions) {
	page := ctx.QueryInt("page")
	if page <= 1 {
		page = 1
	}

	var (
		tickets []*models.Ticket
		count   int64
		err     error
	)

	keyword := ctx.Query("q")
	if len(keyword) == 0 {
		tickets, err = opts.Ranger(page, opts.PageSize)
		if err != nil {
			ctx.Handle(500, "opts.Ranger", err)
			return
		}
		count = opts.Counter()
	} else {
		tickets, count, err = models.SearchTicketByName(&models.SearchTicketOptions{
			Keyword:  keyword,
			Status:   opts.Status,
			OrderBy:  opts.OrderBy,
			OwnerId:  opts.OwnerId,
			Page:     page,
			PageSize: opts.PageSize,
		})

		if err != nil {
			ctx.Handle(500, "SearchTicketByName", err)
			return
		}
	}

	ctx.Data["Keyword"] = keyword
	ctx.Data["Total"] = count
	ctx.Data["Page"] = paginater.New(int(count), opts.PageSize, page, 5)
	ctx.Data["Tickets"] = tickets

	ctx.HTML(200, opts.TplName)
}

func ExploreTickets(ctx *context.Context) {
	ctx.Data["Title"] = ctx.Tr("admin.tickets.explorer")
	ctx.Data["PageIsAdminTickets"] = true

	RenderTicketSearch(ctx, &TicketSearchOptions{
		Counter:  models.CountTickets,
		Ranger:   models.Tickets,
		PageSize: setting.ExplorePagingNum,
		OrderBy:  "created_unix DESC",
		TplName:  TICKET_LIST,
	})
}

func prepareTicketStatus(ctx *context.Context) {
	ctx.Data["TicketStatus"] = models.GetTicketStatus()
}

func NewTicket(ctx *context.Context) {
	ctx.Data["Title"] = ctx.Tr("admin.tickets.new_ticket")
	ctx.Data["PageIsAdminTicket"] = true
	ctx.Data["RequireSimpleMDE"] = true

	prepareTicketStatus(ctx)
	prepareCategories(ctx)
	prepareProjects(ctx)

	ctx.HTML(200, TICKET_NEW)
}

func NewTicketPost(ctx *context.Context, form validate.AdminCreateTicketForm) {
	ctx.Data["Title"] = ctx.Tr("admin.tickets.new_ticket")
	ctx.Data["PageIsAdminTicket"] = true
	ctx.Data["RequireSimpleMDE"] = true

	prepareTicketStatus(ctx)
	prepareCategories(ctx)
	prepareProjects(ctx)

	p, err := models.GetProjectByIDS(form.ProjectUID)
	if err != nil {
		if !models.IsErrProjectNotExist(err) {
			ctx.Handle(400, "GetProjectByIDS", err)
		}
	}

	if ctx.HasError() {
		ctx.HTML(200, TICKET_NEW)
		return
	}

	t := &models.Ticket{
		Name:        form.Name,
		Description: form.Description,
		IsNotify:    form.IsNotify,
		Status:      form.Status,
		CommitHash:  form.CommitHash,
		CategoryUID: form.CategoryUID,
		CreatedBy:   ctx.User.ID,
	}

	if err := models.CreateTicket(t, p); err != nil {
		ctx.Handle(500, "CreateTicket", err)
		return
	}

	log.Trace("Ticket created by admin (%s): %s", ctx.User.Username, t.Name)

	ctx.Flash.Success(ctx.Tr("admin.tickets.create_success"))
	ctx.Redirect(setting.AppSubUrl + "/admin/tickets")

}

func prepareTicketInfo(ctx *context.Context) (*models.Ticket, error) {
	c, err := models.GetTicketByID(ctx.ParamsInt64(":ticketid"))
	if err != nil {
		if !models.IsErrTicketNotExist(err) {
			ctx.Handle(500, "GetTicketByID", err)
		}

		return nil, err
	}
	ctx.Data["Ticket"] = c

	return c, nil
}

func EditTicket(ctx *context.Context) {
	ctx.Data["Title"] = ctx.Tr("admin.tickets.edit_ticket")
	ctx.Data["PageIsAdminTicket"] = true
	ctx.Data["RequireSimpleMDE"] = true

	prepareTicketStatus(ctx)
	prepareCategories(ctx)
	prepareProjects(ctx)

	_, err := prepareTicketInfo(ctx)
	if err != nil && models.IsErrTicketNotExist(err) {
		ctx.Flash.Success(ctx.Tr("admin.tickets.not_found"))
		ctx.Redirect(setting.AppSubUrl + "/admin/tickets/")
		return
	}

	if ctx.Written() {
		return
	}

	ctx.HTML(200, TICKET_EDIT)
}

func EditTicketPost(ctx *context.Context, form validate.AdminEditTicketForm) {
	ctx.Data["Title"] = ctx.Tr("admin.tickets.edit_ticket")
	ctx.Data["PageIsAdminTicket"] = true
	ctx.Data["RequireSimpleMDE"] = true

	prepareTicketStatus(ctx)
	prepareCategories(ctx)
	prepareProjects(ctx)

	p, err := models.GetProjectByIDS(form.ProjectUID)
	if err != nil {
		if !models.IsErrProjectNotExist(err) {
			ctx.Handle(400, "GetProjectByIDS", err)
		}
	}

	t, nil := prepareTicketInfo(ctx)
	if ctx.Written() {
		return
	}

	t.Name = form.Name
	t.Description = form.Description
	t.IsNotify = form.IsNotify
	t.Status = form.Status
	t.CommitHash = form.CommitHash
	t.CategoryUID = form.CategoryUID
	t.UpdatedBy = ctx.User.ID

	if err := models.UpdateTicket(t, p); err != nil {
		ctx.Handle(500, "UpdateTicket", err)
		return
	}
	log.Trace("Tickets updated by admin (%s): %s", ctx.User.Username, t.Name)

	ctx.Flash.Success(ctx.Tr("admin.tickets.update_success"))
	ctx.Redirect(setting.AppSubUrl + "/admin/tickets")
}

func DeleteTicketPost(ctx *context.Context) {

	c, err := models.GetTicketByID(ctx.QueryInt64("id"))
	if err != nil {
		if models.IsErrTicketNotExist(err) {
			ctx.Flash.Error(ctx.Tr("admin.tickets.not_found"))
			ctx.JSON(200, map[string]interface{}{
				"redirect": setting.AppSubUrl + "/admin/tickets",
			})
			return
		}

		ctx.Handle(500, "GetTicketByID", err)
		return
	}

	if err = models.DeleteTicket(c); err != nil {
		ctx.Handle(500, "DeleteTicket", err)
		return
	}
	log.Trace("Ticket deleted by admin (%s): %s", ctx.User.Username, c.Name)

	ctx.Flash.Success(ctx.Tr("admin.tickets.delete_success"))
	ctx.JSON(200, map[string]interface{}{
		"redirect": setting.AppSubUrl + "/admin/tickets",
	})

}
