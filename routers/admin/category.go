package admin

import (
	"github.com/Unknwon/paginater"

	"github.com/rlopes-fixeads/redLine/models"
	"github.com/rlopes-fixeads/redLine/modules/base"
	"github.com/rlopes-fixeads/redLine/modules/context"
	"github.com/rlopes-fixeads/redLine/modules/log"
	"github.com/rlopes-fixeads/redLine/modules/setting"
	"github.com/rlopes-fixeads/redLine/modules/validate"
)

const (
	CATEGORY_LIST base.TplName = "admin/category/explorer"
	CATEGORY_EDIT base.TplName = "admin/category/edit"
	CATEGORY_NEW  base.TplName = "admin/category/new"
)

type CategorySearchOptions struct {
	Counter  func() int64
	Ranger   func(int, int) ([]*models.Category, error)
	OrderBy  string
	OwnerId  int64
	Status   int
	TplName  base.TplName
	Page     int
	PageSize int
}

func RenderCategorySearch(ctx *context.Context, opts *CategorySearchOptions) {
	page := ctx.QueryInt("page")
	if page <= 1 {
		page = 1
	}

	var (
		categories []*models.Category
		count      int64
		err        error
	)

	keyword := ctx.Query("q")
	if len(keyword) == 0 {
		categories, err = opts.Ranger(page, opts.PageSize)
		if err != nil {
			ctx.Handle(500, "opts.Ranger", err)
			return
		}
		count = opts.Counter()
	} else {
		categories, count, err = models.SearchCategoryByName(&models.SearchCategoryOptions{
			Keyword:  keyword,
			Status:   opts.Status,
			OrderBy:  opts.OrderBy,
			Page:     page,
			PageSize: opts.PageSize,
		})

		if err != nil {
			ctx.Handle(500, "SearchCategoryByName", err)
			return
		}
	}

	ctx.Data["Keyword"] = keyword
	ctx.Data["Total"] = count
	ctx.Data["Page"] = paginater.New(int(count), opts.PageSize, page, 5)
	ctx.Data["Categories"] = categories

	ctx.HTML(200, opts.TplName)
}

func ExploreCategories(ctx *context.Context) {
	ctx.Data["Title"] = ctx.Tr("admin.categories.explorer")
	ctx.Data["PageIsAdminCategories"] = true

	RenderCategorySearch(ctx, &CategorySearchOptions{
		Counter:  models.CountCategories,
		Ranger:   models.Categories,
		PageSize: setting.ExplorePagingNum,
		OrderBy:  "created_unix ASC",
		TplName:  CATEGORY_LIST,
	})
}

func prepareCategories(ctx *context.Context) {
	var err error
	ctx.Data["Categories"], err = models.AllCategories()
	if err != nil {
		ctx.Data["Categories"] = make([]models.Category, 0)
	}
}

func NewCategory(ctx *context.Context) {
	ctx.Data["Title"] = ctx.Tr("admin.categories.new_category")
	ctx.Data["PageIsAdminCategory"] = true

	ctx.HTML(200, CATEGORY_NEW)
}

func NewCategoryPost(ctx *context.Context, form validate.AdminCreateCategoryForm) {
	ctx.Data["Title"] = ctx.Tr("admin.categories.new_category")
	ctx.Data["PageIsAdminCategory"] = true

	if ctx.HasError() {
		ctx.HTML(200, CATEGORY_NEW)
		return
	}

	c := &models.Category{
		Name:        form.Name,
		Description: form.Description,
		IsActive:    form.IsActive,
	}

	if err := models.CreateCategory(c); err != nil {
		ctx.Handle(500, "CreateCategory", err)
		return
	}

	log.Trace("Category created by admin (%s): %s", ctx.User.Username, c.Name)

	ctx.Flash.Success(ctx.Tr("admin.categories.create_success"))
	ctx.Redirect(setting.AppSubUrl + "/admin/categories")
}

func prepareCategoryInfo(ctx *context.Context) (*models.Category, error) {
	c, err := models.GetCategoryByID(ctx.ParamsInt64(":categoryid"))
	if err != nil {
		if !models.IsErrCategoryNotExist(err) {
			ctx.Handle(500, "GetCategoryByID", err)
		}

		return nil, err
	}
	ctx.Data["Category"] = c

	return c, nil
}

func EditCategory(ctx *context.Context) {
	ctx.Data["Title"] = ctx.Tr("admin.categories.edit_category")
	ctx.Data["PageIsAdminCategory"] = true

	_, err := prepareCategoryInfo(ctx)
	if err != nil && models.IsErrCategoryNotExist(err) {
		ctx.Flash.Success(ctx.Tr("admin.categories.not_found"))
		ctx.Redirect(setting.AppSubUrl + "/admin/categories/")
		return
	}

	if ctx.Written() {
		return
	}

	ctx.HTML(200, CATEGORY_EDIT)
}

func EditCategoryPost(ctx *context.Context, form validate.AdminEditCategoryForm) {
	ctx.Data["Title"] = ctx.Tr("admin.categories.edit_category")
	ctx.Data["PageIsAdminCategory"] = true

	t, nil := prepareCategoryInfo(ctx)
	if ctx.Written() {
		return
	}

	t.Name = form.Name
	t.Description = form.Description
	t.IsActive = form.IsActive

	if err := models.UpdateCategory(t); err != nil {
		ctx.Handle(500, "UpdateCategory", err)
		return
	}
	log.Trace("Categories updated by admin (%s): %s", ctx.User.Username, t.Name)

	ctx.Flash.Success(ctx.Tr("admin.categories.update_success"))
	ctx.Redirect(setting.AppSubUrl + "/admin/categories")

}

func DeleteCategoryPost(ctx *context.Context) {
	c, err := models.GetCategoryByID(ctx.QueryInt64("id"))
	if err != nil {
		if models.IsErrCategoryNotExist(err) {
			ctx.Flash.Error(ctx.Tr("admin.categories.not_found"))
			ctx.JSON(200, map[string]interface{}{
				"redirect": setting.AppSubUrl + "/admin/users",
			})
			return
		}

		ctx.Handle(500, "GetCategoryByID", err)
		return
	}

	if err = models.DeleteCategory(c); err != nil {
		switch {
		case models.IsErrCategoryHasTicketAssociated(err):
			ctx.Flash.Error(ctx.Tr("admin.categories.delete_has_tickets"))
			ctx.JSON(200, map[string]interface{}{
				"redirect": setting.AppSubUrl + "/admin/users/" + ctx.Query(":userid"),
			})
		default:
			ctx.Handle(500, "DeleteCategory", err)
		}
		return
	}
	log.Trace("Category deleted by admin (%s): %s", ctx.User.Username, c.Name)

	ctx.Flash.Success(ctx.Tr("admin.categories.delete_success"))
	ctx.JSON(200, map[string]interface{}{
		"redirect": setting.AppSubUrl + "/admin/categories",
	})
}
