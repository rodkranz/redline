package admin

import (
	"github.com/rlopes-fixeads/redLine/modules/base"
	"github.com/rlopes-fixeads/redLine/modules/context"
)

const (
	DASHBOARD base.TplName = "admin/dashboard"
)

func Dashboard(ctx *context.Context) {
	ctx.Data["Title"] = ctx.Tr("admin.dashboard")
	ctx.Data["PageIsAdmin"] = true
	ctx.Data["PageIsAdminDashboard"] = true

	ctx.HTML(200, DASHBOARD)
}
