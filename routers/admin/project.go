package admin

import (
	"github.com/Unknwon/paginater"

	"github.com/rlopes-fixeads/redLine/models"
	"github.com/rlopes-fixeads/redLine/modules/base"
	"github.com/rlopes-fixeads/redLine/modules/context"
	"github.com/rlopes-fixeads/redLine/modules/log"
	"github.com/rlopes-fixeads/redLine/modules/setting"
	"github.com/rlopes-fixeads/redLine/modules/validate"
)

const (
	PROJECT_LIST  base.TplName = "admin/project/explorer"
	PROJECT_EDIT  base.TplName = "admin/project/edit"
	PROJECT_NEW   base.TplName = "admin/project/new"
	PROJECT_USERS base.TplName = "admin/project/users"
)

type ProjectSearchOptions struct {
	Counter  func() int64
	Ranger   func(int, int) ([]*models.Project, error)
	OrderBy  string
	OwnerId  int64
	Status   int
	TplName  base.TplName
	Page     int
	PageSize int
}

func RenderProjectSearch(ctx *context.Context, opts *ProjectSearchOptions) {
	page := ctx.QueryInt("page")
	if page <= 1 {
		page = 1
	}

	var (
		projects []*models.Project
		count    int64
		err      error
	)

	keyword := ctx.Query("q")
	if len(keyword) == 0 {
		projects, err = opts.Ranger(page, opts.PageSize)
		if err != nil {
			ctx.Handle(500, "opts.Ranger", err)
			return
		}
		count = opts.Counter()
	} else {
		projects, count, err = models.SearchProjectByName(&models.SearchProjectOptions{
			Keyword:  keyword,
			Status:   opts.Status,
			OrderBy:  opts.OrderBy,
			Page:     page,
			PageSize: opts.PageSize,
		})

		if err != nil {
			ctx.Handle(500, "SearchProjectByName", err)
			return
		}
	}

	ctx.Data["Keyword"] = keyword
	ctx.Data["Total"] = count
	ctx.Data["Page"] = paginater.New(int(count), opts.PageSize, page, 5)
	ctx.Data["Projects"] = projects

	ctx.HTML(200, opts.TplName)
}

func ExploreProjects(ctx *context.Context) {
	ctx.Data["Title"] = ctx.Tr("admin.projects.explorer")
	ctx.Data["PageIsAdminProjects"] = true

	RenderProjectSearch(ctx, &ProjectSearchOptions{
		Counter:  models.CountProjects,
		Ranger:   models.Projects,
		PageSize: setting.ExplorePagingNum,
		OrderBy:  "created_unix ASC",
		TplName:  PROJECT_LIST,
	})
}

func prepareProjects(ctx *context.Context) {
	var err error
	ctx.Data["Projects"], err = models.GetAllProjects()
	if err != nil {
		ctx.Data["Projects"] = make([]models.Project, 0)
	}
}

func NewProject(ctx *context.Context) {
	ctx.Data["Title"] = ctx.Tr("admin.projects.new_project")
	ctx.Data["PageIsAdminProject"] = true

	ctx.HTML(200, PROJECT_NEW)
}

func NewProjectPost(ctx *context.Context, form validate.AdminCreateProjectForm) {
	ctx.Data["Title"] = ctx.Tr("admin.projects.new_project")
	ctx.Data["PageIsAdminProject"] = true

	if ctx.HasError() {
		ctx.HTML(200, PROJECT_NEW)
		return
	}

	c := &models.Project{
		Name:        form.Name,
		Description: form.Description,
		IsActive:    form.IsActive,
	}

	if err := models.CreateProject(c); err != nil {
		ctx.Handle(500, "CreateProject", err)
		return
	}

	log.Trace("Project created by admin (%s): %s", ctx.User.Username, c.Name)

	ctx.Flash.Success(ctx.Tr("admin.projects.create_success"))
	ctx.Redirect(setting.AppSubUrl + "/admin/projects")
}

func prepareProjectInfo(ctx *context.Context) (*models.Project, error) {
	c, err := models.GetProjectByID(ctx.ParamsInt64(":projectid"))
	if err != nil {
		if !models.IsErrProjectNotExist(err) {
			ctx.Handle(500, "GetProjectByID", err)
		}

		return nil, err
	}
	ctx.Data["Project"] = c

	return c, nil
}

func prepareProjectsFromUser(ctx *context.Context) {
	u, p, err := models.GetProjectsWithAssociationUser(ctx.ParamsInt64(":userid"))
	if err != nil {
		if !models.IsErrUserNotExist(err) {
			ctx.Handle(500, "GetUserByID", err)
		}
		return
	}

	ctx.Data["User"] = u
	ctx.Data["UserProjects"] = p
}

func prepareProjectsUserNotAssociated(ctx *context.Context) {
	u, p, err := models.GetProjectsWithoutAssociationUser(ctx.ParamsInt64(":userid"))
	if err != nil {
		if !models.IsErrUserNotExist(err) {
			ctx.Handle(500, "GetUserByID", err)
		}
		return
	}

	ctx.Data["User"] = u
	ctx.Data["UnassociatedProjects"] = p
}

func EditProject(ctx *context.Context) {
	ctx.Data["Title"] = ctx.Tr("admin.projects.edit_project")
	ctx.Data["PageIsAdminProject"] = true

	_, err := prepareProjectInfo(ctx)
	if err != nil && models.IsErrProjectNotExist(err) {
		ctx.Flash.Success(ctx.Tr("admin.projects.not_found"))
		ctx.Redirect(setting.AppSubUrl + "/admin/projects/")
		return
	}

	if ctx.Written() {
		return
	}

	ctx.HTML(200, PROJECT_EDIT)
}

func EditProjectPost(ctx *context.Context, form validate.AdminEditProjectForm) {
	ctx.Data["Title"] = ctx.Tr("admin.projects.edit_project")
	ctx.Data["PageIsAdminProject"] = true

	t, nil := prepareProjectInfo(ctx)
	if ctx.Written() {
		return
	}

	t.Name = form.Name
	t.Description = form.Description
	t.IsActive = form.IsActive

	if err := models.UpdateProject(t); err != nil {
		ctx.Handle(500, "UpdateProject", err)
		return
	}
	log.Trace("Projects updated by admin (%s): %s", ctx.User.Username, t.Name)

	ctx.Flash.Success(ctx.Tr("admin.projects.update_success"))
	ctx.Redirect(setting.AppSubUrl + "/admin/projects")

}

func DeleteProjectPost(ctx *context.Context) {

}

func UsersFromProjectGet(ctx *context.Context) {
	ctx.Data["Title"] = ctx.Tr("admin.projects.users_of_project")
	ctx.Data["PageIsAdminProject"] = true

	ctx.HTML(200, PROJECT_USERS)
}
