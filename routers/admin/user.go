package admin

import (
	"github.com/Unknwon/paginater"

	"github.com/Unknwon/com"
	"github.com/rlopes-fixeads/redLine/models"
	"github.com/rlopes-fixeads/redLine/modules/base"
	"github.com/rlopes-fixeads/redLine/modules/context"
	"github.com/rlopes-fixeads/redLine/modules/log"
	"github.com/rlopes-fixeads/redLine/modules/setting"
	"github.com/rlopes-fixeads/redLine/modules/validate"
)

const (
	USER_LIST     base.TplName = "admin/user/explorer"
	USER_EDIT     base.TplName = "admin/user/edit"
	USER_NEW      base.TplName = "admin/user/new"
	USER_PROJECTS base.TplName = "admin/user/projects"
)

type UserSearchOptions struct {
	Counter  func() int64
	Ranger   func(int, int) ([]*models.User, error)
	OrderBy  string
	Type     models.UserType
	TplName  base.TplName
	Page     int
	PageSize int
}

func RenderUserSearch(ctx *context.Context, opts *UserSearchOptions) {
	page := ctx.QueryInt("page")
	if page <= 1 {
		page = 1
	}

	var (
		users []*models.User
		count int64
		err   error
	)

	keyword := ctx.Query("q")
	if len(keyword) == 0 {
		users, err = opts.Ranger(page, opts.PageSize)
		if err != nil {
			ctx.Handle(500, "opts.Ranger", err)
			return
		}
		count = opts.Counter()
	} else {
		users, count, err = models.SearchUserByName(&models.SearchUserOptions{
			Keyword:  keyword,
			OrderBy:  opts.OrderBy,
			Type:     opts.Type,
			Page:     page,
			PageSize: opts.PageSize,
		})

		if err != nil {
			ctx.Handle(500, "SearchUserByName", err)
			return
		}
	}

	ctx.Data["Keyword"] = keyword
	ctx.Data["Total"] = count
	ctx.Data["Page"] = paginater.New(int(count), opts.PageSize, page, 5)
	ctx.Data["Users"] = users

	ctx.HTML(200, opts.TplName)
}

func ExploreUsers(ctx *context.Context) {
	ctx.Data["Title"] = ctx.Tr("admin.users.explorer")
	ctx.Data["PageIsAdminUsers"] = true

	RenderUserSearch(ctx, &UserSearchOptions{
		Counter:  models.CountUsers,
		Ranger:   models.Users,
		PageSize: setting.ExplorePagingNum,
		OrderBy:  "username ASC",
		TplName:  USER_LIST,
	})
}

func NewUser(ctx *context.Context) {
	ctx.Data["Title"] = ctx.Tr("admin.users.new_account")
	ctx.Data["PageIsAdminUser"] = true

	ctx.HTML(200, USER_NEW)
}

func NewUserPost(ctx *context.Context, form validate.AdminCreateUserForm) {
	ctx.Data["Title"] = ctx.Tr("admin.users.new_account")
	ctx.Data["PageIsAdminUser"] = true

	if ctx.HasError() {
		ctx.HTML(200, USER_NEW)
		return
	}
	u := &models.User{
		FullName:    form.FullName,
		Username:    form.Username,
		Email:       form.Email,
		Passwd:      form.Password,
		IsActive:    form.IsActive,
		IsAdmin:     form.IsAdmin,
		Description: form.Description,
	}

	if err := models.CreateUser(u); err != nil {
		switch {
		case models.IsErrUserAlreadyExist(err):
			ctx.Data["Err_UserName"] = true
			ctx.RenderWithErr(ctx.Tr("form.username_been_taken"), USER_NEW, &form)
		case models.IsErrEmailAlreadyUsed(err):
			ctx.Data["Err_Email"] = true
			ctx.RenderWithErr(ctx.Tr("form.email_been_used"), USER_NEW, &form)
		case models.IsErrNameReserved(err):
			ctx.Data["Err_UserName"] = true
			ctx.RenderWithErr(ctx.Tr("user.form.name_reserved", err.(models.ErrNameReserved).Name), USER_NEW, &form)
		case models.IsErrNamePatternNotAllowed(err):
			ctx.Data["Err_UserName"] = true
			ctx.RenderWithErr(ctx.Tr("user.form.name_pattern_not_allowed", err.(models.ErrNamePatternNotAllowed).Pattern), USER_NEW, &form)
		default:
			ctx.Handle(500, "CreateUser", err)
		}
		return
	}

	log.Trace("Account created by admin (%s): %s", ctx.User.Username, u.Username)

	ctx.Flash.Success(ctx.Tr("admin.users.create_success", u.Username))
	ctx.Redirect(setting.AppSubUrl + "/admin/users")
}

func prepareUserInfo(ctx *context.Context) (*models.User, error) {
	u, err := models.GetUserByID(ctx.ParamsInt64(":userid"))
	if err != nil {
		if !models.IsErrUserNotExist(err) {
			ctx.Handle(500, "GetUserByID", err)
		}

		return nil, err
	}
	ctx.Data["User"] = u

	return u, nil
}

func EditUser(ctx *context.Context) {
	ctx.Data["Title"] = ctx.Tr("admin.users.edit_user")
	ctx.Data["PageIsAdminUser"] = true

	_, err := prepareUserInfo(ctx)
	if err != nil && models.IsErrUserNotExist(err) {
		ctx.Flash.Success(ctx.Tr("admin.users.not_found"))
		ctx.Redirect(setting.AppSubUrl + "/admin/users/")
		return
	}

	if ctx.Written() {
		return
	}

	ctx.HTML(200, USER_EDIT)
}

func EditUserPost(ctx *context.Context, form validate.AdminEditUserForm) {
	ctx.Data["Title"] = ctx.Tr("admin.users.edit_account")
	ctx.Data["PageIsAdminUser"] = true

	u, _ := prepareUserInfo(ctx)
	if ctx.Written() {
		return
	}

	if ctx.HasError() {
		ctx.HTML(200, USER_EDIT)
		return
	}

	if len(form.Password) > 0 {
		u.Passwd = form.Password
		u.Salt = models.GetUserSalt()
		u.EncodePasswd()
	}

	u.Username = form.Username
	u.FullName = form.FullName
	u.Email = form.Email
	u.IsActive = form.IsActive
	u.IsAdmin = form.IsAdmin
	u.Description = form.Description

	if err := models.UpdateUser(u); err != nil {
		if models.IsErrEmailAlreadyUsed(err) {
			ctx.Data["Err_Email"] = true
			ctx.RenderWithErr(ctx.Tr("form.email_been_used"), USER_EDIT, &form)
		} else {
			ctx.Handle(500, "UpdateUser", err)
		}
		return
	}
	log.Trace("Account profile updated by admin (%s): %s", ctx.User.Username, u.Username)

	ctx.Flash.Success(ctx.Tr("admin.users.update_success", u.Username))
	ctx.Redirect(setting.AppSubUrl + "/admin/users")
}

func DeleteUserPost(ctx *context.Context) {
	u, err := models.GetUserByID(ctx.QueryInt64("id"))
	if err != nil {
		if models.IsErrUserNotExist(err) {
			ctx.Flash.Error(ctx.Tr("admin.users.not_found"))
			ctx.JSON(200, map[string]interface{}{
				"redirect": setting.AppSubUrl + "/admin/users",
			})
			return
		}

		ctx.Handle(500, "GetUserByID", err)
		return
	}

	if err = models.DeleteUser(u); err != nil {
		ctx.Handle(500, "DeleteUser", err)
		return
	}
	log.Trace("User deleted by admin (%s): %s", ctx.User.Username, u.Username)

	ctx.Flash.Success(ctx.Tr("admin.users.delete_success", u.Username))
	ctx.JSON(200, map[string]interface{}{
		"redirect": setting.AppSubUrl + "/admin/users",
	})
}

func UsersProjectsGet(ctx *context.Context) {
	ctx.Data["Title"] = ctx.Tr("admin.project.user_projects")
	ctx.Data["PageIsAdminUser"] = true

	prepareProjectsFromUser(ctx)
	prepareProjectsUserNotAssociated(ctx)
	if ctx.Written() {
		return
	}

	ctx.HTML(200, USER_PROJECTS)
}

func UsersProjectsPost(ctx *context.Context, form validate.AdminUserAssociateProjectForm) {
	ctx.Data["Title"] = ctx.Tr("admin.users.user_projects")
	ctx.Data["PageIsAdminUser"] = true

	prepareProjectsFromUser(ctx)
	prepareProjectsUserNotAssociated(ctx)

	if ctx.HasError() {
		ctx.HTML(200, USER_PROJECTS)
		return
	}

	userProject := &models.UsersProjects{
		UserID:    ctx.ParamsInt64(":userid"),
		ProjectID: form.ProjectUID,
		CreatedBy: ctx.User.ID,
	}

	if err := models.CreateUsersProjects(userProject); err != nil {
		switch {
		case models.IsErrUserNotExist(err):
			ctx.RenderWithErr(ctx.Tr("admin.users.not_found"), USER_PROJECTS, &form)
		case models.IsErrProjectNotExist(err):
			ctx.RenderWithErr(ctx.Tr("admin.projects.not_found"), USER_PROJECTS, &form)
		default:
			ctx.Handle(500, "CreateAssociation", err)
		}
		return
	}

	p, _ := models.GetProjectByID(form.ProjectUID)
	log.Trace("Project asssociated created by admin (%s): %s", ctx.User.Username, p.Name)

	ctx.Flash.Success(ctx.Tr("admin.projects.create_asssociated_success"))
	ctx.Redirect(setting.AppSubUrl + "/admin/users/" + com.ToStr(ctx.ParamsInt64(":userid")) + "/projects")
}

func DeleteUsersProjectsPost(ctx *context.Context) {
	c, err := models.GetUsersProjectsByID(ctx.QueryInt64("id"))
	if err != nil {
		ctx.Flash.Error(ctx.Tr("admin.projects.not_found"))
		ctx.JSON(200, map[string]interface{}{
			"redirect": setting.AppSubUrl + "/admin/users/" + com.ToStr(ctx.ParamsInt64(":userid")) + "/projects",
		})
		//ctx.Handle(500, "GetTicketByID", err)
		return
	}

	if err = models.DeleteUsersProjects(c); err != nil {
		ctx.Handle(500, "DeleteTicket", err)
		return
	}
	log.Trace("Association deleted by admin (%s)", ctx.User.Username)

	ctx.Flash.Success(ctx.Tr("admin.projects.delete_asssociated_success"))
	ctx.JSON(200, map[string]interface{}{
		"redirect": setting.AppSubUrl + "/admin/users/" + com.ToStr(ctx.ParamsInt64(":userid")) + "/projects",
	})
}
