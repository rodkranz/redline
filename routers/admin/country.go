package admin

import (
	"github.com/Unknwon/paginater"

	"github.com/rlopes-fixeads/redLine/models"
	"github.com/rlopes-fixeads/redLine/modules/base"
	"github.com/rlopes-fixeads/redLine/modules/context"
	"github.com/rlopes-fixeads/redLine/modules/log"
	"github.com/rlopes-fixeads/redLine/modules/setting"
	"github.com/rlopes-fixeads/redLine/modules/validate"
)

const (
	COUNTRY_LIST base.TplName = "admin/country/explorer"
	COUNTRY_NEW  base.TplName = "admin/country/new"
	COUNTRY_EDIT base.TplName = "admin/country/edit"
)

type CountrySearchOptions struct {
	Counter  func() int64
	Ranger   func(int, int) ([]*models.Country, error)
	OrderBy  string
	TplName  base.TplName
	Page     int
	PageSize int
}

func RenderCountrySearch(ctx *context.Context, opts *CountrySearchOptions) {
	page := ctx.QueryInt("page")
	if page <= 1 {
		page = 1
	}

	var (
		countries []*models.Country
		count     int64
		err       error
	)

	keyword := ctx.Query("q")
	if len(keyword) == 0 {
		countries, err = opts.Ranger(page, opts.PageSize)
		if err != nil {
			ctx.Handle(500, "opts.Ranger", err)
			return
		}
		count = opts.Counter()
	} else {
		countries, count, err = models.SearchCountryByName(&models.SearchCountryOptions{
			Keyword:  keyword,
			OrderBy:  opts.OrderBy,
			Page:     page,
			PageSize: opts.PageSize,
		})

		if err != nil {
			ctx.Handle(500, "SearchCountryByName", err)
			return
		}
	}

	ctx.Data["Keyword"] = keyword
	ctx.Data["Total"] = count
	ctx.Data["Page"] = paginater.New(int(count), opts.PageSize, page, 5)
	ctx.Data["Countries"] = countries

	ctx.HTML(200, opts.TplName)
}

func ExploreCountries(ctx *context.Context) {
	ctx.Data["Title"] = ctx.Tr("admin.countries.explorer")
	ctx.Data["PageIsAdminCountries"] = true

	RenderCountrySearch(ctx, &CountrySearchOptions{
		Counter:  models.CountCountries,
		Ranger:   models.Countries,
		PageSize: setting.ExplorePagingNum,
		OrderBy:  "name ASC",
		TplName:  COUNTRY_LIST,
	})
}

func NewCountry(ctx *context.Context) {
	ctx.Data["Title"] = ctx.Tr("admin.countries.new_country")
	ctx.Data["PageIsAdminCountry"] = true

	ctx.HTML(200, COUNTRY_NEW)
}

func NewCountryPost(ctx *context.Context, form validate.AdminCreateCountryForm) {
	ctx.Data["Title"] = ctx.Tr("admin.countries.new_country")
	ctx.Data["PageIsAdminCountry"] = true

	if ctx.HasError() {
		ctx.HTML(200, COUNTRY_NEW)
		return
	}

	c := &models.Country{
		Name:      form.Name,
		ShortName: form.ShortName,
		IsActive:  form.IsActive,
	}

	if err := models.CreateCountry(c); err != nil {
		switch {
		case models.IsErrCountryAlreadyExist(err):
			ctx.Data["Err_Name"] = true
			ctx.RenderWithErr(ctx.Tr("form.country_already_registread"), COUNTRY_NEW, &form)
		default:
			ctx.Handle(500, "CreateCountry", err)
		}
		return
	}
	log.Trace("Country created by admin (%s): %s", ctx.User.Username, c.Name)

	ctx.Flash.Success(ctx.Tr("admin.countries.create_success"))
	ctx.Redirect(setting.AppSubUrl + "/admin/countries")
}

func prepareCountryInfo(ctx *context.Context) (*models.Country, error) {
	c, err := models.GetCountryByID(ctx.ParamsInt64(":countryid"))
	if err != nil {
		if !models.IsErrCountryNotExist(err) {
			ctx.Handle(500, "GetCountryByID", err)
		}

		return nil, err
	}
	ctx.Data["Country"] = c

	return c, nil
}

func EditCountry(ctx *context.Context) {
	ctx.Data["Title"] = ctx.Tr("admin.countries.edit_country")
	ctx.Data["PageIsAdminCountry"] = true

	_, err := prepareCountryInfo(ctx)
	if err != nil && models.IsErrCountryNotExist(err) {
		ctx.Flash.Success(ctx.Tr("admin.countries.not_found"))
		ctx.Redirect(setting.AppSubUrl + "/admin/countries/")
		return
	}

	if ctx.Written() {
		return
	}

	ctx.HTML(200, COUNTRY_EDIT)
}

func EditCountryPost(ctx *context.Context, form validate.AdminEditCountryForm) {
	ctx.Data["Title"] = ctx.Tr("admin.countries.edit_country")
	ctx.Data["PageIsAdminCountry"] = true

	c, nil := prepareCountryInfo(ctx)
	if ctx.Written() {
		return
	}

	if ctx.HasError() {
		ctx.HTML(200, COUNTRY_EDIT)
		return
	}

	c.Name = form.Name
	c.ShortName = form.ShortName
	c.IsActive = form.IsActive

	if err := models.UpdateCountry(c); err != nil {
		if models.IsErrCountryAlreadyExist(err) {
			ctx.Data["Err_Shortname"] = true
			ctx.Data["Err_Name"] = true
			ctx.RenderWithErr(ctx.Tr("form.country_already_registread"), COUNTRY_EDIT, &form)
		} else {
			ctx.Handle(500, "UpdateCountry", err)
		}
		return
	}
	log.Trace("Countries updated by admin (%s): %s", ctx.User.Username, c.Name)

	ctx.Flash.Success(ctx.Tr("admin.countries.update_success"))
	ctx.Redirect(setting.AppSubUrl + "/admin/countries")

}

func DeleteCountryPost(ctx *context.Context) {
	c, err := models.GetCountryByID(ctx.QueryInt64("id"))
	if err != nil {
		if models.IsErrCountryNotExist(err) {
			ctx.Flash.Error(ctx.Tr("admin.countries.not_found"))
			ctx.JSON(200, map[string]interface{}{
				"redirect": setting.AppSubUrl + "/admin/countries",
			})
			return
		}

		ctx.Handle(500, "GetCountryByID", err)
		return
	}

	if err = models.DeleteCountry(c); err != nil {
		ctx.Handle(500, "DeleteCountry", err)
		return
	}
	log.Trace("Country deleted by admin (%s): %s", ctx.User.Username, c.Name)

	ctx.Flash.Success(ctx.Tr("admin.countries.delete_success"))
	ctx.JSON(200, map[string]interface{}{
		"redirect": setting.AppSubUrl + "/admin/countries",
	})
}
