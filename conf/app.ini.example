APP_NAME = RedLine
APP_DESCRIPTION = Red Line implementation

RUN_MODE = dev

[server]
PROTOCOL  = http
DOMAIN    = 192.168.5.183
ROOT_URL  = %(PROTOCOL)s://%(DOMAIN)s:%(HTTP_PORT)s/
HTTP_ADDR =
HTTP_PORT = 3000
LOCAL_ROOT_URL = http://%(DOMAIN):%(HTTP_PORT)s/
CERT_FILE = custom/https/cert.pem
KEY_FILE = custom/https/key.pem
STATIC_ROOT_PATH = ./
APP_DATA_PATH = data
ENABLE_GZIP  = true
LANDING_PAGE = home

[database]
; Either "mysql", "postgres" or "sqlite3", it's your choice
DB_TYPE = postgres
HOST = 127.0.0.1:5432
NAME = redline
USER = digo
PASSWD =

;DB_TYPE = mysql
;HOST = 127.0.0.1:33060
;NAME = redline
;USER = homstead
;PASSWD = secret

; For "postgres" only, either "disable", "require" or "verify-full"
SSL_MODE = disable
; For "sqlite3" and "tidb", use absolute path when you start as service
PATH = data/redline.db

[API]
allowCrossDomain = true

[admin]

[security]
INSTALL_LOCK         = false
SECRET_KEY           = !#@FDEWREWR&*(
LOGIN_REMEMBER_DAYS  = 7
COOKIE_USERNAME      = redline_awesome
COOKIE_REMEMBER_NAME = redline_incredible

REVERSE_PROXY_AUTHENTICATION_USER = X-WEBAUTH-USER

[service]
ACTIVE_CODE_LIVE_MINUTES = 180
RESET_PASSWD_CODE_LIVE_MINUTES = 180
REGISTER_EMAIL_CONFIRM = false
DISABLE_REGISTRATION = false
REQUIRE_SIGNIN_VIEW = false
ENABLE_NOTIFY_MAIL = false
ENABLE_CAPTCHA = true

[mailer]
ENABLED = false
SEND_BUFFER_LEN = 100
SUBJECT = %(APP_NAME)s
HOST =
DISABLE_HELO =
HELO_HOSTNAME =
SKIP_VERIFY =
USE_CERTIFICATE = false
CERT_FILE = custom/mailer/cert.pem
KEY_FILE = custom/mailer/key.pem
FROM =
USER =
PASSWD =

[ui]
EXPLORE_PAGING_NUM = 10

[markdown]
; Enable hard line break extension
ENABLE_HARD_LINE_BREAK = false
; List of custom URL-Schemes that are allowed as links when rendering Markdown
; for example git,magnet
CUSTOM_URL_SCHEMES =

[cache]
; Either "memory", "redis", or "memcache", default is "memory"
ADAPTER = memory
; For "memory" only, GC interval in seconds, default is 60
INTERVAL = 60
; For "redis" and "memcache", connection host address
; redis: network=tcp,addr=:6379,password=macaron,db=0,pool_size=100,idle_timeout=180
; memcache: `127.0.0.1:11211`
HOST =

[session]
; Either "memory", "file", "redis" or "mysql", default is "memory"
;PROVIDER = memory
PROVIDER = memory
; Provider config options
; memory: not have any config yet
; file: session file path, e.g. `data/sessions`
; redis: network=tcp,addr=:6379,password=macaron,db=0,pool_size=100,idle_timeout=180
PROVIDER_CONFIG = data/sessions
; Session cookie name
COOKIE_NAME = i_like_redlines
; If you use session in https only, default is false
COOKIE_SECURE = false
; Enable set cookie, default is true
ENABLE_SET_COOKIE = true
; Session GC time interval, default is 86400
GC_INTERVAL_TIME = 86400
; Session life time, default is 86400
SESSION_LIFE_TIME = 86400

[attachment]
; Whether attachments are enabled. Defaults to `true`
ENABLE = true
; Path for attachments. Defaults to `data/attachments`
PATH = data/attachments
; One or more allowed types, e.g. image/jpeg|image/png
ALLOWED_TYPES = image/jpeg|image/png
; Max size of each file. Defaults to 32MB
MAX_SIZE = 4
; Max number of files per upload. Defaults to 10
MAX_FILES = 5

[time]
; Specifies the format for fully outputed dates. Defaults to RFC1123
; Special supported values are ANSIC, UnixDate, RubyDate, RFC822, RFC822Z, RFC850, RFC1123, RFC1123Z, RFC3339, RFC3339Nano, Kitchen, Stamp, StampMilli, StampMicro and StampNano
; For more information about the format see http://golang.org/pkg/time/#pkg-constants
FORMAT =

[log]
ROOT_PATH = /var/log/redline
; Either "console", "file", "conn", "smtp" or "database", default is "console"
; Use comma to separate multiple modes, e.g. "console, file"
MODE = console
; Buffer length of channel, keep it as it is if you don't know what it is.
BUFFER_LEN = 10000
; Either "Trace", "Debug", "Info", "Warn", "Error", "Critical", default is "Trace"
LEVEL = Debug

; For "console" mode only
[log.console]
LEVEL =

; For "file" mode only
[log.file]
LEVEL =
; This enables automated log rotate(switch of following options), default is true
LOG_ROTATE = true
; Max line number of single file, default is 1000000
MAX_LINES = 1000000
; Max size shift of single file, default is 28 means 1 << 28, 256MB
MAX_SIZE_SHIFT = 28
; Segment log daily, default is true
DAILY_ROTATE = true
; Expired days of log file(delete after max days), default is 7
MAX_DAYS = 7

; For "conn" mode only
[log.conn]
LEVEL =
; Reconnect host for every single message, default is false
RECONNECT_ON_MSG = false
; Try to reconnect when connection is lost, default is false
RECONNECT = false
; Either "tcp", "unix" or "udp", default is "tcp"
PROTOCOL = tcp
; Host address
ADDR =

; For "smtp" mode only
[log.smtp]
LEVEL =
; Name displayed in mail title, default is "Diagnostic message from server"
SUBJECT = Diagnostic message from server
; Mail server
HOST =
; Mailer user name and password
USER =
PASSWD =
; Receivers, can be one or more, e.g. ["1@example.com","2@example.com"]
RECEIVERS =

; For "database" mode only
[log.database]
LEVEL =
; Either "mysql" or "postgres"
DRIVER =
; Based on xorm, e.g.: root:root@localhost/redline?charset=utf8
CONN =

[cron]
; Enable running cron tasks periodically.
ENABLED = true
; Run cron tasks when redline starts.
RUN_AT_START = false

; Update mirrors
[cron.update_mirrors]
SCHEDULE = @every 1h

; Repository health check
[cron.repo_health_check]
SCHEDULE = @every 24h
TIMEOUT = 60s
; Arguments for command 'git fsck', e.g. "--unreachable --tags"
; see more on http://git-scm.com/docs/git-fsck/1.7.5
ARGS =

; Check repository statistics
[cron.check_repo_stats]
RUN_AT_START = true
SCHEDULE = @every 24h

[git]
MAX_GIT_DIFF_LINES = 10000
; Arguments for command 'git gc', e.g. "--aggressive --auto"
; see more on http://git-scm.com/docs/git-gc/1.7.5
GC_ARGS =

; Operation timeout in seconds
[git.timeout]
MIGRATE = 600
MIRROR = 300
CLONE = 300
PULL = 300

[i18n]
LANGS = en-GB,pt-BR
NAMES = English,Português do Brasil

; Used for datetimepicker
[i18n.datelang]
en-GB = en-GB
pt-BR = pt-BR

; Extension mapping to highlight class
; e.g. .toml=ini
[highlight.mapping]

[other]
SHOW_FOOTER_BRANDING = false
; Show version information about red line and go in the footer
SHOW_FOOTER_VERSION = true
