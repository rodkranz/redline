package models

import (
	"fmt"
	"strings"
	"time"

	"github.com/go-xorm/xorm"

	"github.com/rlopes-fixeads/redLine/modules/log"
	"github.com/rlopes-fixeads/redLine/modules/setting"
)

type Country struct {
	ID        int64
	Name      string
	ShortName string

	IsActive bool `xorm:"NOT NULL DEFAULT true"`

	Created_by  int64
	Created     time.Time `xorm:"-"`
	CreatedUnix int64
	Updated     time.Time `xorm:"-"`
	UpdatedUnix int64
}

func (t *Country) BeforeInsert() {
	t.CreatedUnix = time.Now().UTC().Unix()
}

func (t *Country) BeforeUpdate() {
	t.UpdatedUnix = time.Now().UTC().Unix()
}

func (t *Country) AfterSet(colName string, _ xorm.Cell) {
	switch colName {
	case "created_unix":
		t.Created = time.Unix(t.CreatedUnix, 0).Local()
	case "updated_unix":
		t.Updated = time.Unix(t.UpdatedUnix, 0).Local()
	}
}

func IsCountryExist(uid int64, name string) (bool, error) {
	if len(name) == 0 {
		return false, nil
	}

	return x.Where("id!=?", uid).Get(&Country{Name: strings.ToLower(name)})
}

func countCountries(e Engine) int64 {
	count, _ := e.Where("1=1").Count(new(Country))
	return count
}

// CountUsers returns number of users.
func CountCountries() int64 {
	return countCountries(x)
}

// Users returns number of users in given page.
func Countries(page, pageSize int) ([]*Country, error) {
	countries := make([]*Country, 0, pageSize)
	return countries, x.Limit(pageSize, (page-1)*pageSize).Asc("id").Find(&countries)
}

// CreateCountry creates record of a new country.
func CreateCountry(c *Country) (err error) {
	isExist, err := IsCountryExist(0, c.Name)
	if err != nil {
		return err
	} else if isExist {
		return ErrCountryAlreadyExist{c.Name}
	}

	c.Name = strings.ToLower(c.Name)

	sess := x.NewSession()
	defer sess.Close()
	if err = sess.Begin(); err != nil {
		return err
	}

	if _, err = sess.Insert(c); err != nil {
		sess.Rollback()
		return err
	}

	return sess.Commit()
}

func updateCountry(e Engine, c *Country) error {

	_, err := e.Id(c.ID).AllCols().Update(c)
	return err
}

func UpdateCountry(c *Country) error {
	return updateCountry(x, c)
}

func getCountryByID(e Engine, id int64) (*Country, error) {
	c := new(Country)
	has, err := e.Id(id).Get(c)
	if err != nil {
		return nil, err
	} else if !has {
		return nil, ErrCountryNotExist{id, ""}
	}
	return c, nil
}

func GetCountryByName(name string) (*Country, error) {
	if len(name) == 0 {
		return nil, ErrCountryNotExist{0, name}
	}

	c := &Country{Name: strings.ToLower(name)}

	has, err := x.Get(c)
	if err != nil {
		return nil, err
	} else if !has {
		return nil, ErrCountryNotExist{0, name}
	}

	return c, nil
}

func GetCountryByID(id int64) (*Country, error) {
	return getCountryByID(x, id)
}

func deleteCountry(e *xorm.Session, c *Country) error {
	if _, err := e.Id(c.ID).Delete(new(Country)); err != nil {
		return fmt.Errorf("Delete: %v", err)
	}

	return nil
}

func DeleteCountry(c *Country) (err error) {
	sess := x.NewSession()
	defer sessionRelease(sess)
	if err = sess.Begin(); err != nil {
		return err
	}

	if err = deleteCountry(sess, c); err != nil {
		// Note: don't wrapper error here.
		return err
	}

	return sess.Commit()
}

type SearchCountryOptions struct {
	Keyword  string
	OrderBy  string
	Page     int
	PageSize int
}

func SearchCountryByName(opts *SearchCountryOptions) (countries []*Country, _ int64, _ error) {
	log.Debug("OK")

	if len(opts.Keyword) == 0 {
		return countries, 0, nil
	}
	opts.Keyword = strings.ToLower(opts.Keyword)

	if opts.PageSize <= 0 || opts.PageSize > setting.ExplorePagingNum {
		opts.PageSize = setting.ExplorePagingNum
	}
	if opts.Page <= 0 {
		opts.Page = 1
	}

	searchQuery := "%" + opts.Keyword + "%"
	countries = make([]*Country, 0, opts.PageSize)
	// Append conditions
	sess := x.Where("LOWER(name) LIKE ?", searchQuery).
		Or("LOWER(short_name) LIKE ?", searchQuery)

	var countSess xorm.Session
	countSess = *sess
	count, err := countSess.Count(new(Country))
	if err != nil {
		return nil, 0, fmt.Errorf("Count: %v", err)
	}

	if len(opts.OrderBy) > 0 {
		sess.OrderBy(opts.OrderBy)
	}
	return countries, count, sess.Limit(opts.PageSize, (opts.Page-1)*opts.PageSize).Find(&countries)
}
