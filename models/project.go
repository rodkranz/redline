package models

import (
	"fmt"
	"strings"
	"time"

	"github.com/go-xorm/xorm"

	"github.com/rlopes-fixeads/redLine/modules/setting"
)

type Project struct {
	ID          int64  `xorm:"pk autoincr"`
	Name        string `xorm:"VARCHAR(255)"`
	Description string `xorm:"TEXT"`

	IsActive bool `xorm:"NOT NULL DEFAULT true"`

	Status int

	OwnerUID int64

	Created     time.Time `xorm:"-"`
	CreatedUnix int64

	Updated     time.Time `xorm:"-"`
	UpdatedUnix int64
}

func (t *Project) BeforeInsert() {
	t.CreatedUnix = time.Now().UTC().Unix()
}

func (t *Project) BeforeUpdate() {
	t.UpdatedUnix = time.Now().UTC().Unix()
}

func (t *Project) AfterSet(colName string, _ xorm.Cell) {
	switch colName {
	case "created_unix":
		t.Created = time.Unix(t.CreatedUnix, 0).Local()
	case "updated_unix":
		t.Updated = time.Unix(t.UpdatedUnix, 0).Local()
	}
}

func (t *Project) GetOwner() *User {
	u, e := GetUserByID(t.OwnerUID)
	if e != nil {
		return &User{FullName: "System", Username: "sys"}
	}
	return u
}

func countProjects(e Engine) int64 {
	count, _ := e.Where("1=1").Count(new(Project))
	return count
}

func CountProjects() int64 {
	return countProjects(x)
}

func GetAllProjects() ([]*Project, error) {
	projects := []*Project{}
	return projects, x.Asc("id").Where("is_active = ?", true).Find(&projects)
}

func Projects(page, pageSize int) ([]*Project, error) {
	projects := make([]*Project, 0, pageSize)
	return projects, x.Limit(pageSize, (page-1)*pageSize).Asc("id").Find(&projects)
}

type SearchProjectOptions struct {
	Keyword  string
	Status   int
	OrderBy  string
	Page     int
	PageSize int
}

func SearchProjectByName(opts *SearchProjectOptions) (categories []*Project, _ int64, _ error) {
	if len(opts.Keyword) == 0 {
		return categories, 0, nil
	}
	opts.Keyword = strings.ToLower(opts.Keyword)

	if opts.PageSize <= 0 || opts.PageSize > setting.ExplorePagingNum {
		opts.PageSize = setting.ExplorePagingNum
	}
	if opts.Page <= 0 {
		opts.Page = 1
	}

	searchQuery := "%" + opts.Keyword + "%"
	categories = make([]*Project, 0, opts.PageSize)

	// Append conditions
	sess := x.Where("LOWER(name) LIKE ?", searchQuery).
		Or("LOWER(description) LIKE ?", searchQuery)

	if opts.Status > 0 {
		sess.And("status = ?", opts.Status)
	}

	var countSess xorm.Session
	countSess = *sess
	count, err := countSess.Count(new(Project))
	if err != nil {
		return nil, 0, fmt.Errorf("Count: %v", err)
	}

	if len(opts.OrderBy) > 0 {
		sess.OrderBy(opts.OrderBy)
	}

	return categories, count, sess.Limit(opts.PageSize, (opts.Page-1)*opts.PageSize).Find(&categories)
}

func CreateProject(t *Project) (err error) {
	sess := x.NewSession()
	defer sess.Close()
	if err = sess.Begin(); err != nil {
		return err
	}

	if _, err = sess.Insert(t); err != nil {
		sess.Rollback()
		return err
	}

	return sess.Commit()
}

func updateProject(e Engine, t *Project) error {
	_, err := e.Id(t.ID).AllCols().Update(t)
	return err
}

func UpdateProject(t *Project) error {
	return updateProject(x, t)
}

func getProjectByID(e Engine, id int64) (*Project, error) {
	t := new(Project)
	has, err := e.Id(id).Get(t)
	if err != nil {
		return nil, err
	} else if !has {
		return nil, ErrProjectNotExist{id, ""}
	}
	return t, nil
}

func getProjectByIDS(e Engine, ids []int64) ([]*Project, error) {
	ps := make([]*Project, 0)
	if len(ids) == 0 {
		return ps, nil
	}
	return ps, x.In("id", ids).Find(&ps)
}

func GetProjectByName(name string) (*Project, error) {
	if len(name) == 0 {
		return nil, ErrProjectNotExist{0, name}
	}

	t := &Project{Name: strings.ToLower(name)}

	has, err := x.Get(t)
	if err != nil {
		return nil, err
	} else if !has {
		return nil, ErrProjectNotExist{0, name}
	}

	return t, nil
}

func GetProjectByID(id int64) (*Project, error) {
	return getProjectByID(x, id)
}

func GetProjectByIDS(id []int64) ([]*Project, error) {
	return getProjectByIDS(x, id)
}

func deleteProject(e *xorm.Session, t *Project) error {
	if _, err := e.Id(t.ID).Delete(new(Project)); err != nil {
		return fmt.Errorf("Delete: %v", err)
	}

	return nil
}

func DeleteProject(c *Project) (err error) {
	sess := x.NewSession()
	defer sessionRelease(sess)
	if err = sess.Begin(); err != nil {
		return err
	}

	if err = deleteProject(sess, c); err != nil {
		return err
	}

	return sess.Commit()
}
