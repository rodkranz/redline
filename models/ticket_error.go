package models

import "fmt"

/**
 * Tickets
 */
type ErrTicketNotExist struct {
	UID  int64
	Name string
}

func IsErrTicketNotExist(err error) bool {
	_, ok := err.(ErrTicketNotExist)
	return ok
}

func (err ErrTicketNotExist) Error() string {
	return fmt.Sprintf("ticket does not exist [uid: %d, name: %s]", err.UID, err.Name)
}

/**
 * TicketsProject
 */
type ErrTicketsProjectsNotExist struct {
	TicketID  int64
	ProjectID int64
}

func IsErrTicketsProjectsNotExist(err error) bool {
	_, ok := err.(ErrTicketsProjectsNotExist)
	return ok
}

func (err ErrTicketsProjectsNotExist) Error() string {
	return fmt.Sprintf("ticket and project does not exist [projectId: %d, ticketId: %s]", err.ProjectID, err.TicketID)
}
