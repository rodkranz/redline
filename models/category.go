package models

import (
	"fmt"
	"strings"
	"time"

	"github.com/go-xorm/xorm"

	"github.com/rlopes-fixeads/redLine/modules/setting"
)

type Category struct {
	ID          int64  `xorm:"pk autoincr"`
	Name        string `xorm:"VARCHAR(255)"`
	Description string `xorm:"TEXT"`

	IsActive bool `xorm:"NOT NULL DEFAULT true"`

	Created     time.Time `xorm:"-"`
	CreatedUnix int64

	Updated     time.Time `xorm:"-"`
	UpdatedUnix int64
}

func (t *Category) BeforeInsert() {
	t.CreatedUnix = time.Now().UTC().Unix()
}

func (t *Category) BeforeUpdate() {
	t.UpdatedUnix = time.Now().UTC().Unix()
}

func (t *Category) AfterSet(colName string, _ xorm.Cell) {
	switch colName {
	case "created_unix":
		t.Created = time.Unix(t.CreatedUnix, 0).Local()
	case "updated_unix":
		t.Updated = time.Unix(t.UpdatedUnix, 0).Local()
	}
}

func countCategories(e Engine) int64 {
	count, _ := e.Where("1=1").Count(new(Category))
	return count
}

func CountCategories() int64 {
	return countCategories(x)
}

func Categories(page, pageSize int) ([]*Category, error) {
	categories := make([]*Category, 0, pageSize)
	return categories, x.Limit(pageSize, (page-1)*pageSize).Asc("id").Find(&categories)
}

func AllCategories() ([]*Category, error) {
	categories := []*Category{}
	return categories, x.Asc("id").Find(&categories)
}

type SearchCategoryOptions struct {
	Keyword  string
	Status   int
	OrderBy  string
	Page     int
	PageSize int
}

func SearchCategoryByName(opts *SearchCategoryOptions) (categories []*Category, _ int64, _ error) {
	if len(opts.Keyword) == 0 {
		return categories, 0, nil
	}
	opts.Keyword = strings.ToLower(opts.Keyword)

	if opts.PageSize <= 0 || opts.PageSize > setting.ExplorePagingNum {
		opts.PageSize = setting.ExplorePagingNum
	}
	if opts.Page <= 0 {
		opts.Page = 1
	}

	searchQuery := "%" + opts.Keyword + "%"
	categories = make([]*Category, 0, opts.PageSize)

	// Append conditions
	sess := x.Where("LOWER(name) LIKE ?", searchQuery).
		Or("LOWER(description) LIKE ?", searchQuery)

	if opts.Status > 0 {
		sess.And("status = ?", opts.Status)
	}

	var countSess xorm.Session
	countSess = *sess
	count, err := countSess.Count(new(Category))
	if err != nil {
		return nil, 0, fmt.Errorf("Count: %v", err)
	}

	if len(opts.OrderBy) > 0 {
		sess.OrderBy(opts.OrderBy)
	}

	return categories, count, sess.Limit(opts.PageSize, (opts.Page-1)*opts.PageSize).Find(&categories)
}

// CreateCountry creates record of a new country.
func CreateCategory(t *Category) (err error) {
	sess := x.NewSession()
	defer sess.Close()
	if err = sess.Begin(); err != nil {
		return err
	}

	if _, err = sess.Insert(t); err != nil {
		sess.Rollback()
		return err
	}

	return sess.Commit()
}

func updateCategory(e Engine, t *Category) error {
	_, err := e.Id(t.ID).AllCols().Update(t)
	return err
}

func UpdateCategory(t *Category) error {
	return updateCategory(x, t)
}

func getCategoryByID(e Engine, id int64) (*Category, error) {
	t := new(Category)
	has, err := e.Id(id).Get(t)
	if err != nil {
		return nil, err
	} else if !has {
		return nil, ErrCategoryNotExist{id, ""}
	}
	return t, nil
}

func GetCategoryByName(name string) (*Category, error) {
	if len(name) == 0 {
		return nil, ErrCategoryNotExist{0, name}
	}

	t := &Category{Name: strings.ToLower(name)}

	has, err := x.Get(t)
	if err != nil {
		return nil, err
	} else if !has {
		return nil, ErrCategoryNotExist{0, name}
	}

	return t, nil
}

func GetCategoryByID(id int64) (*Category, error) {
	return getCategoryByID(x, id)
}

func deleteCategory(e *xorm.Session, t *Category) error {
	if _, err := e.Id(t.ID).Delete(new(Category)); err != nil {
		return fmt.Errorf("Delete: %v", err)
	}

	return nil
}

func DeleteCategory(c *Category) (err error) {
	if l, _ := GetTicketByCategoryUID(c.ID); len(l) > 0 {
		return ErrCategoryHasTicketAssociated{c.Name, len(l)}
	}

	sess := x.NewSession()
	defer sessionRelease(sess)
	if err = sess.Begin(); err != nil {
		return err
	}

	if err = deleteCategory(sess, c); err != nil {
		return err
	}

	return sess.Commit()
}
