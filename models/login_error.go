package models

import "fmt"

/**
 * Authentication
 */

type ErrAuthenticationNotExist struct {
	ID int64
}

func IsErrAuthenticationNotExist(err error) bool {
	_, ok := err.(ErrAuthenticationNotExist)
	return ok
}

func (err ErrAuthenticationNotExist) Error() string {
	return fmt.Sprintf("authentication does not exist [id: %d]", err.ID)
}

type ErrNameReserved struct {
	Name string
}

func IsErrNameReserved(err error) bool {
	_, ok := err.(ErrNameReserved)
	return ok
}

func (err ErrNameReserved) Error() string {
	return fmt.Sprintf("name is reserved [name: %s]", err.Name)
}

type ErrNamePatternNotAllowed struct {
	Pattern string
}

func IsErrNamePatternNotAllowed(err error) bool {
	_, ok := err.(ErrNamePatternNotAllowed)
	return ok
}

func (err ErrNamePatternNotAllowed) Error() string {
	return fmt.Sprintf("name pattern is not allowed [pattern: %s]", err.Pattern)
}

type ErrPasswordInvalid struct {
}

func IsErrPasswordInvalid(err error) bool {
	_, ok := err.(ErrPasswordInvalid)
	return ok
}

func (err ErrPasswordInvalid) Error() string {
	return fmt.Sprintf("Password or user invalid")
}
