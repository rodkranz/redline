package models

import "fmt"

/**
 * Country
 */

type ErrCountryAlreadyExist struct {
	Name string
}

func IsErrCountryAlreadyExist(err error) bool {
	_, ok := err.(ErrCountryAlreadyExist)
	return ok
}

func (err ErrCountryAlreadyExist) Error() string {
	return fmt.Sprintf("country name has been used [name: %s]", err.Name)
}

type ErrCountryNotExist struct {
	UID  int64
	Name string
}

func IsErrCountryNotExist(err error) bool {
	_, ok := err.(ErrCountryNotExist)
	return ok
}

func (err ErrCountryNotExist) Error() string {
	return fmt.Sprintf("user does not exist [uid: %d, name: %s]", err.UID, err.Name)
}
