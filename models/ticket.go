package models

import (
	"fmt"
	"strings"
	"time"

	"github.com/go-xorm/xorm"

	"github.com/rlopes-fixeads/redLine/modules/log"
	"github.com/rlopes-fixeads/redLine/modules/setting"
)

const (
	TICKET_STATUS_PENDING  int = 1
	TICKET_STATUS_ACTIVE   int = 2
	TICKET_STATUS_ARCHIVED int = 3
)

type Ticket struct {
	ID          int64  `xorm:"pk autoincr"`
	Name        string `xorm:"VARCHAR(255)"`
	Description string `xorm:"TEXT"`
	CommitHash  string `xorm:"VARCHAR(255)"`

	CategoryUID int64 `xorm:"INDEX NOT NULL"`

	IsNotify bool `xorm:"NOT NULL DEFAULT false"`

	Status int `xorm:"NOT NULL DEFAULT 1"`

	CreatedBy   int64     `xorm:"INDEX NOT NULL"`
	Created     time.Time `xorm:"-"`
	CreatedUnix int64

	UpdatedBy   int64     `xorm:"INDEX"`
	Updated     time.Time `xorm:"-"`
	UpdatedUnix int64

	DeletedBy   int64     `xorm:"INDEX"`
	Deleted     time.Time `xorm:"-"`
	DeletedUnix int64
}

func (t *Ticket) BeforeInsert() {
	t.CreatedUnix = time.Now().UTC().Unix()
}

func (t *Ticket) BeforeUpdate() {
	t.UpdatedUnix = time.Now().UTC().Unix()
}

func (t *Ticket) AfterSet(colName string, _ xorm.Cell) {
	switch colName {
	case "created_unix":
		t.Created = time.Unix(t.CreatedUnix, 0).Local()
	case "updated_unix":
		t.Updated = time.Unix(t.UpdatedUnix, 0).Local()
	}
}

func (t *Ticket) GetOwner() *User {
	user, err := GetUserByID(t.CreatedBy)
	if err != nil {
		return nil
	}

	return user
}

func (t *Ticket) CanSee(userID int64) bool {
	p := t.GetProjectsForUser(userID)
	return len(p) > 0
}

func (t *Ticket) CanEdit(userID int64) bool {
	ps := t.GetProjects()

	for _, p := range ps {
		if p.Status == TICKET_PROJECT_STATUS_DONE {
			return false
		}
	}

	return true
}

func (t *Ticket) GetCategory() *Category {
	c, err := GetCategoryByID(t.CategoryUID)
	if err != nil {
		return new(Category)
	}

	return c
}

func (t *Ticket) GetProjects() []*Project {
	var projects = make([]*Project, 0)
	_, p, err := GetProjectsByTicketID(t.ID)

	if err != nil {
		return projects
	}

	return p
}

func (t *Ticket) GetProjectsForUser(userId int64) []*Project {
	var projects = make([]*Project, 0)
	_, pst, err := GetProjectsByTicketID(t.ID)
	if err != nil {
		return projects
	}

	u, pswa, err := GetProjectsWithAssociationUser(userId)
	if err != nil {
		return projects
	}

	// if user is admin return all projects
	if u.IsAdmin {
		return pst
	}

	// filter project by user access
	for _, p := range pst {
		for _, pa := range pswa {
			if p.ID == pa.ProjectID {
				projects = append(projects, p)
			}
		}
	}

	return projects
}

func (t *Ticket) HasProjectID(projectID int64) bool {
	i, err := x.Where("project_id = ?", projectID).And("ticket_id = ?", t.ID).Count(new(TicketsProjects))

	if err != nil {
		return false
	}

	return (i > 0)
}

func countTickets(e Engine) int64 {
	count, _ := e.Where("1=1").Count(new(User))
	return count
}

func CountTickets() int64 {
	return countTickets(x)
}

func Tickets(page, pageSize int) ([]*Ticket, error) {
	tickets := make([]*Ticket, 0, pageSize)
	return tickets, x.Limit(pageSize, (page-1)*pageSize).Desc("id").Find(&tickets)
}

type SearchTicketOptions struct {
	Keyword  string
	Status   int
	OrderBy  string
	OwnerId  int64
	Page     int
	PageSize int
}

func SearchTicketByName(opts *SearchTicketOptions) (tickets []*Ticket, _ int64, _ error) {
	if len(opts.Keyword) == 0 {
		return tickets, 0, nil
	}
	opts.Keyword = strings.ToLower(opts.Keyword)

	if opts.PageSize <= 0 || opts.PageSize > setting.ExplorePagingNum {
		opts.PageSize = setting.ExplorePagingNum
	}
	if opts.Page <= 0 {
		opts.Page = 1
	}

	searchQuery := "%" + opts.Keyword + "%"
	tickets = make([]*Ticket, 0, opts.PageSize)

	// Append conditions
	sess := x.Where("LOWER(name) LIKE ?", searchQuery).
		Or("LOWER(description) LIKE ?", searchQuery)

	var countSess xorm.Session
	countSess = *sess
	count, err := countSess.Count(new(Ticket))
	if err != nil {
		return nil, 0, fmt.Errorf("Count: %v", err)
	}

	if opts.Status > 0 {
		sess.And("status = ?", opts.Status)
	}

	if opts.OwnerId > 0 {
		sess.And("order_by = ?", opts.OwnerId)
	}

	if len(opts.OrderBy) > 0 {
		sess.OrderBy(opts.OrderBy)
	} else {
		sess.OrderBy("created_unix DESC")
	}

	return tickets, count, sess.Limit(opts.PageSize, (opts.Page-1)*opts.PageSize).Find(&tickets)
}

func CreateTicket(t *Ticket, ps []*Project) (err error) {
	sess := x.NewSession()
	defer sess.Close()
	if err = sess.Begin(); err != nil {
		return err
	}

	if _, err = sess.Insert(t); err != nil {
		sess.Rollback()
		return err
	}

	for _, p := range ps {
		tp := &TicketsProjects{
			TicketID:  t.ID,
			ProjectID: p.ID,
			CreatedBy: t.CreatedBy,
			Status:    TICKET_PROJECT_STATUS_PENDING,
		}

		if _, err = sess.Insert(tp); err != nil {
			sess.Rollback()
			return err
		}
	}

	return sess.Commit()
}

func updateTicket(e Engine, t *Ticket, ps []*Project) (err error) {
	sess := x.NewSession()
	defer sess.Close()
	if err = sess.Begin(); err != nil {
		return err
	}

	_, tps, err := getProjectsByTicketID(t.ID)
	listStatus := make(map[int64]int, len(tps))
	if err != nil {
		return err
	}
	for _, p := range tps {
		listStatus[p.ID] = p.Status
	}

	for _, p := range t.GetProjects() {
		_, err := sess.Where("project_id = ?", p.ID).And("ticket_id = ?", t.ID).Delete(new(TicketsProjects))
		if err != nil {
			sess.Rollback()
			return err
		}
	}

	if _, err := sess.Id(t.ID).AllCols().Update(t); err != nil {
		sess.Rollback()
		return err
	}

	for _, p := range ps {
		if _, has := listStatus[p.ID]; !has {
			listStatus[p.ID] = 0
		}

		tp := &TicketsProjects{
			TicketID:  t.ID,
			ProjectID: p.ID,
			UpdatedBy: t.UpdatedBy,
			Status:    listStatus[p.ID],
		}

		log.Info("insert: %v", tp)
		if _, err = sess.Insert(tp); err != nil {
			sess.Rollback()
			return err
		}
	}

	return sess.Commit()
}

func UpdateTicket(t *Ticket, ps []*Project) error {
	return updateTicket(x, t, ps)
}

func getTicketByID(e Engine, id int64) (*Ticket, error) {
	t := new(Ticket)
	has, err := e.Id(id).Get(t)
	if err != nil {
		return nil, err
	} else if !has {
		return nil, ErrTicketNotExist{id, ""}
	}
	return t, nil
}

func GetTicketByName(name string) (*Ticket, error) {
	if len(name) == 0 {
		return nil, ErrTicketNotExist{0, name}
	}

	t := &Ticket{Name: strings.ToLower(name)}

	has, err := x.Get(t)
	if err != nil {
		return nil, err
	} else if !has {
		return nil, ErrTicketNotExist{0, name}
	}

	return t, nil
}

func GetTicketByID(id int64) (*Ticket, error) {
	return getTicketByID(x, id)
}

func deleteTicket(e *xorm.Session, t *Ticket) error {
	if _, err := e.Id(t.ID).Delete(new(Ticket)); err != nil {
		return fmt.Errorf("Delete: %v", err)
	}

	return nil
}

func DeleteTicket(c *Ticket) (err error) {
	sess := x.NewSession()
	defer sessionRelease(sess)
	if err = sess.Begin(); err != nil {
		return err
	}

	if err = deleteTicket(sess, c); err != nil {
		return err
	}

	return sess.Commit()
}

func GetTicketStatus() map[int]string {
	statuses := make(map[int]string, 3)

	statuses[TICKET_STATUS_PENDING] = "admin.tickets.status_pending"
	statuses[TICKET_STATUS_ACTIVE] = "admin.tickets.status_active"
	statuses[TICKET_STATUS_ARCHIVED] = "admin.tickets.status_archived"

	return statuses
}

func getTicketByCategoryUID(categoryId int64, e Engine) ([]*Ticket, error) {
	tickets := []*Ticket{}
	return tickets, x.Where("category_uid=?", categoryId).Asc("id").Find(&tickets)
}

func GetTicketByCategoryUID(categoryId int64) ([]*Ticket, error) {
	return getTicketByCategoryUID(categoryId, x)
}

func getTicketByProjectUID(ProjectUID int64, e Engine) ([]*Ticket, error) {
	tickets := []*Ticket{}
	return tickets, x.Where("project_uid=?", ProjectUID).Asc("id").Find(&tickets)
}

func GetTicketByProjectUID(ProjectUID int64) ([]*Ticket, error) {
	return getTicketByProjectUID(ProjectUID, x)
}

func GetProjects(t *Ticket) ([]*Project, error) {
	_, p, err := GetProjectsByTicketID(t.ID)
	if err != nil {
		return nil, err
	}

	return p, nil
}
