package models

import (
	"strings"
	"time"

	"github.com/go-xorm/core"
)

type LoginType int

type LoginSource struct {
	ID        int64 `xorm:"pk autoincr"`
	Type      LoginType
	Name      string          `xorm:"UNIQUE"`
	IsActived bool            `xorm:"NOT NULL DEFAULT false"`
	Cfg       core.Conversion `xorm:"TEXT"`

	Created     time.Time `xorm:"-"`
	CreatedUnix int64
	Updated     time.Time `xorm:"-"`
	UpdatedUnix int64
}

func UserSignIn(uname, passwd string) (*User, *AccessToken, error) {
	var u *User
	if strings.Contains(uname, "@") {
		u = &User{Email: strings.ToLower(uname)}
	} else {
		u = &User{Username: strings.ToLower(uname)}
	}

	userExists, err := x.Get(u)
	if err != nil {
		return nil, nil, err
	}

	if userExists {
		if !u.ValidatePassword(passwd) {
			return nil, nil, ErrPasswordInvalid{}
		}

		at := &AccessToken{
			UID:  u.ID,
			Name: "Api",
		}

		if err = NewAccessToken(at); err != nil {
			return nil, nil, err
		}

		return u, at, nil
	}
	return nil, nil, ErrUserNotExist{u.ID, uname}
}
