package models

import (
	"fmt"
	"time"

	"github.com/go-xorm/xorm"
)

type UsersProjects struct {
	UID int64 `xorm:"pk autoincr"`

	UserID    int64 `xorm:"INDEX NOT NULL"`
	ProjectID int64 `xorm:"INDEX NOT NULL"`

	CreatedBy   int64     `xorm:"INDEX NOT NULL"`
	Created     time.Time `xorm:"-"`
	CreatedUnix int64
}

func (up *UsersProjects) BeforeInsert() {
	up.CreatedUnix = time.Now().UTC().Unix()
}

type UserProjectsAssociation struct {
	UsersProjects `xorm:"extends"`
	User          `xorm:"extends"`
	Project       `xorm:"extends"`
}

func getProjectsWithAssociateUser(userID int64, isAssociated bool) (*User, []*UserProjectsAssociation, error) {
	u, err := GetUserByID(userID)
	if err != nil {
		return nil, nil, err
	}

	userProjects := make([]*UserProjectsAssociation, 0)
	s := x.Table("users_projects").
		Select("users_projects.*, project.*").
		Join("INNER", "project", "project.id = users_projects.project_id").
		Where("users_projects.user_id = ?", userID).
		Where("project.is_active = ?", true).
		OrderBy("project.name asc")

	return u, userProjects, s.Find(&userProjects)
}

func getProjectsWithoutAssociateUser(userID int64) (*User, []*Project, error) {
	u, err := GetUserByID(userID)
	if err != nil {
		return nil, nil, err
	}

	ups := make([]*UsersProjects, 0)
	err = x.Where("user_id = ?", userID).Find(&ups)
	if err != nil {
		return nil, nil, err
	}

	projects := make([]*Project, 0)
	sess := x.Table("project")

	for _, up := range ups {
		sess.Where("id != ?", up.ProjectID)
	}

	sess.OrderBy("project.name asc").Where("project.is_active = ?", true)

	return u, projects, sess.Find(&projects)
}

func GetProjectsWithAssociationUser(userID int64) (*User, []*UserProjectsAssociation, error) {
	return getProjectsWithAssociateUser(userID, true)
}

func GetProjectsWithoutAssociationUser(userID int64) (*User, []*Project, error) {
	return getProjectsWithoutAssociateUser(userID)
}

func getUserProject(e Engine, userId, projectId int64) (*UsersProjects, error) {
	u := new(UsersProjects)
	has, err := e.Where("project_id = ? AND user_id = ?", projectId, userId).Get(u)
	if err != nil {
		return nil, err
	} else if !has {
		return nil, ErrAccessNotAllowedProject{userId, projectId}
	}
	return u, nil
}

func GetUserProject(userId, projectId int64) (*UsersProjects, error) {
	return getUserProject(x, userId, projectId)
}

func CreateUsersProjects(up *UsersProjects) (err error) {
	if _, err := GetUserByID(up.UserID); err != nil {
		return err
	}

	if _, err := GetProjectByID(up.ProjectID); err != nil {
		return err
	}

	if _, err := GetUserProject(up.ProjectID, up.UserID); err != nil && !IsErrAccessNotAllowedProject(err) {
		return err
	}

	sess := x.NewSession()
	defer sess.Close()
	if err = sess.Begin(); err != nil {
		return err
	}

	if _, err = sess.Insert(up); err != nil {
		sess.Rollback()
		return err
	}

	return sess.Commit()
}

func getUsersProjectsByID(e Engine, id int64) (*UsersProjects, error) {
	u := new(UsersProjects)
	has, err := e.Id(id).Get(u)
	if err != nil {
		return nil, err
	} else if !has {
		return nil, ErrAssociationNotExist{id}
	}
	return u, nil
}

// GetUserByID returns the user object by given ID if exists.
func GetUsersProjectsByID(id int64) (*UsersProjects, error) {
	return getUsersProjectsByID(x, id)
}

func deleteUsersProjects(e *xorm.Session, u *UsersProjects) error {
	if _, err := e.Id(u.UID).Delete(new(UsersProjects)); err != nil {
		return fmt.Errorf("Delete: %v", err)
	}
	return nil
}

func DeleteUsersProjects(u *UsersProjects) (err error) {
	sess := x.NewSession()
	defer sessionRelease(sess)
	if err = sess.Begin(); err != nil {
		return err
	}

	if err = deleteUsersProjects(sess, u); err != nil {
		// Note: don't wrapper error here.
		return err
	}

	return sess.Commit()
}
