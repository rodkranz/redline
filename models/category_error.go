package models

import "fmt"

/**
 * Category
 */
type ErrCategoryNotExist struct {
	UID  int64
	Name string
}

func IsErrCategoryNotExist(err error) bool {
	_, ok := err.(ErrCategoryNotExist)
	return ok
}

func (err ErrCategoryNotExist) Error() string {
	return fmt.Sprintf("Category does not exist [uid: %d, name: %s]", err.UID, err.Name)
}

type ErrCategoryAlreadyExist struct {
	Name string
}

func IsErrCategoryAlreadyExist(err error) bool {
	_, ok := err.(ErrCategoryAlreadyExist)
	return ok
}

func (err ErrCategoryAlreadyExist) Error() string {
	return fmt.Sprintf("Category already exists [name: %s]", err.Name)
}

type ErrCategoryHasTicketAssociated struct {
	Name          string
	numberTickets int
}

func IsErrCategoryHasTicketAssociated(err error) bool {
	_, ok := err.(ErrCategoryHasTicketAssociated)
	return ok
}

func (err ErrCategoryHasTicketAssociated) Error() string {
	return fmt.Sprintf("This category has tickets associated [name: %s, tickets: %v]", err.Name, err.numberTickets)
}
