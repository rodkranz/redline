package models

import "fmt"

/**
 * Project
 */
type ErrProjectNotExist struct {
	UID  int64
	Name string
}

func IsErrProjectNotExist(err error) bool {
	_, ok := err.(ErrProjectNotExist)
	return ok
}

func (err ErrProjectNotExist) Error() string {
	return fmt.Sprintf("Project does not exist [uid: %d, name: %s]", err.UID, err.Name)
}

type ErrProjectAlreadyExist struct {
	Name string
}

func IsErrProjectAlreadyExist(err error) bool {
	_, ok := err.(ErrProjectAlreadyExist)
	return ok
}

func (err ErrProjectAlreadyExist) Error() string {
	return fmt.Sprintf("Project already exists [name: %s]", err.Name)
}

type ErrAccessNotAllowedProject struct {
	UserUID    int64
	ProjectUID int64
}

func IsErrAccessNotAllowedProject(err error) bool {
	_, ok := err.(ErrAccessNotAllowedProject)
	return ok
}

func (err ErrAccessNotAllowedProject) Error() string {
	return fmt.Sprintf("User have no access to this project [userUID: %v, projectUID: %v]", err.UserUID, err.ProjectUID)
}

type ErrAssociationNotExist struct {
	UID int64
}

func IsErrAssociationNotExist(err error) bool {
	_, ok := err.(ErrAssociationNotExist)
	return ok
}

func (err ErrAssociationNotExist) Error() string {
	return fmt.Sprintf("The Association not exists [uid: %v]", err.UID)
}
