package models

import "fmt"

/**
 * TOKEN
 */
type ErrAccessTokenNotExist struct {
	SHA string
}

func IsErrAccessTokenNotExist(err error) bool {
	_, ok := err.(ErrAccessTokenNotExist)
	return ok
}

func (err ErrAccessTokenNotExist) Error() string {
	return fmt.Sprintf("access token does not exist [sha: %s]", err.SHA)
}
