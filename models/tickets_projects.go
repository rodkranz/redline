package models

import (
	"github.com/go-xorm/xorm"
	"time"
)

const (
	TICKET_PROJECT_STATUS_PENDING int = 1
	TICKET_PROJECT_STATUS_DONE    int = 2
)

type TicketsProjects struct {
	UID int64 `xorm:"pk autoincr"`

	TicketID  int64 `xorm:"INDEX NOT NULL"`
	ProjectID int64 `xorm:"INDEX NOT NULL"`

	Status int `xorm:"NOT NULL DEFAULT 1"`

	Created     time.Time `xorm:"-"`
	CreatedUnix int64
	CreatedBy   int64 `xorm:"INDEX NOT NULL"`

	UpdatedBy   int64     `xorm:"INDEX"`
	Updated     time.Time `xorm:"-"`
	UpdatedUnix int64
}

func (t *TicketsProjects) BeforeInsert() {
	t.CreatedUnix = time.Now().UTC().Unix()
}

func (t *TicketsProjects) BeforeUpdate() {
	t.UpdatedUnix = time.Now().UTC().Unix()
}

func (t *TicketsProjects) AfterSet(colName string, _ xorm.Cell) {
	switch colName {
	case "created_unix":
		t.Created = time.Unix(t.CreatedUnix, 0).Local()
	case "updated_unix":
		t.Updated = time.Unix(t.UpdatedUnix, 0).Local()
	}
}

func CreateTicketProject(tp TicketsProjects) (err error) {
	sess := x.NewSession()
	defer sess.Close()
	if err = sess.Begin(); err != nil {
		return err
	}

	if _, err = sess.Insert(tp); err != nil {
		sess.Rollback()
		return err
	}
	return sess.Commit()

}

type TicketProjectsAssociation struct {
	TicketsProjects `xorm:"extends"`
	Project         `xorm:"extends"`
	Ticket          `xorm:"extends"`
}

func getProjectsByTicketID(ticketID int64) (*Ticket, []*Project, error) {
	t, err := GetTicketByID(ticketID)
	if err != nil {
		return nil, nil, err
	}

	tps := make([]*TicketsProjects, 0)
	err = x.Where("ticket_id = ?", ticketID).Find(&tps)
	if err != nil {
		return nil, nil, err
	}

	projects := make([]*Project, 0, len(tps))
	sess := x.Table("project")

	tmp := make(map[int64]*TicketsProjects, len(tps))
	for _, tp := range tps {
		sess.In("id", tp.ProjectID)
		tmp[tp.ProjectID] = tp
	}

	sess.OrderBy("project.name asc").Where("project.is_active = ?", true)

	err = sess.Find(&projects)
	if err == nil {
		for _, p := range projects {

			if tmp[p.ID] != nil {
				p.OwnerUID = tmp[p.ID].CreatedBy
				if tmp[p.ID].UpdatedBy > 0 {
					p.OwnerUID = tmp[p.ID].UpdatedBy
				}
			}

			p.Status = tmp[p.ID].Status
		}
	}

	return t, projects, err
}

func GetProjectsByTicketID(ticketID int64) (*Ticket, []*Project, error) {
	return getProjectsByTicketID(ticketID)
}

func GetProjectTicketByTicketIdAndProjectId(ticketID, projectID int64, status int) (*TicketsProjects, error) {
	tp := new(TicketsProjects)
	has, err := x.Where("ticket_id = ?", ticketID).And("project_id = ?", projectID).Get(&tp)
	if err != nil {
		return nil, err
	} else if !has {
		return nil, ErrTicketsProjectsNotExist{ticketID, projectID}
	}
	return tp, nil

}

func UpdateStatusProject(ticketID int64, projectIDs []int64, status int, userId int64) error {
	ticketsProjects := make([]*TicketsProjects, 0, len(projectIDs))

	search := x.Where("ticket_id = ?", ticketID)
	for _, id := range projectIDs {
		search.In("project_id", id)
	}

	err := search.Find(&ticketsProjects)
	if err != nil {
		return err
	}

	sess := x.NewSession()
	defer sess.Close()
	if err = sess.Begin(); err != nil {
		return err
	}

	for _, tp := range ticketsProjects {
		tp.Status = status
		tp.UpdatedBy = userId

		if err = updateProjectsTickets(sess, tp); err != nil {
			sess.Rollback()
			return err
		}
	}

	return sess.Commit()
}

func updateProjectsTickets(e Engine, tp *TicketsProjects) error {
	_, err := e.Id(tp.UID).AllCols().Update(tp)
	return err
}

func UpdateProjectsTickets(tp *TicketsProjects) error {
	return updateProjectsTickets(x, tp)
}
