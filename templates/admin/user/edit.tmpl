{{template "base/head" .}}

<div class="main ui container">
    <div class="ui grid">
        <div class="four wide column">
            {{template "admin/base/menu" .}}
        </div>
        <div class="twelve wide column content">

            <!--Title-->
            <div class="ui top attached menu">
                <div class="item"> {{.i18n.Tr "admin.users.edit_user"}} #{{.User.ID}}</div>
                <div class="right menu">
                    <a class="ui icon item" href="{{.AppSubUrl}}/admin/users">
                        <i class="arrow left icon"></i>
                    </a>
                </div>
            </div>
            <!--Title-->

            <div class="ui attached segment">
                {{template "base/alert" .}}

                <form class="ui form" action="{{.Link}}" method="post">
                    {{.CsrfTokenHtml}}

                    <!--Full Name-->
                    <div class="field required {{if .Err_FullName}}error{{end}}">
                        <label for="fullname">{{.i18n.Tr "admin.users.full_name"}}</label>
                        <input type="text"
                               id="fullname"
                               name="full_name"
                               class="form-control"
                               value="{{.User.FullName}}"
                               placeholder="{{.i18n.Tr "admin.users.full_name"}}"
                        required>
                    </div>
                    <!--Full Name-->

                    <!--Username-->
                    <div class="field required {{if .Err_Username}}error{{end}}">
                        <label for="username">{{.i18n.Tr "admin.users.user_name"}}</label>
                        <input type="text"
                               id="username"
                               name="username"
                               class="form-control"
                               value="{{.User.Username}}"
                               placeholder="{{.i18n.Tr "admin.users.user_name"}}"
                        required>
                    </div>
                    <!--Username-->

                    <!--Email-->
                    <div class="field required {{if .Err_Email}}error{{end}}">
                        <label for="email">{{.i18n.Tr "admin.users.email"}}</label>
                        <input type="email"
                               name="email"
                               value="{{.User.Email}}"
                               class="form-control"
                               id="email"
                               placeholder="{{.i18n.Tr "admin.users.email"}}"
                        required>
                    </div>
                    <!--Email-->

                    <!--Password-->
                    <div class="field required {{if .Err_Password}}error{{end}}">
                        <label for="password">{{.i18n.Tr "admin.users.password"}}</label>
                        <input  id="password"
                                type="password"
                                name="password"
                                class="form-control"
                                placeholder="{{.i18n.Tr "admin.users.password"}}">
                    </div>
                    <!--Password-->

                    <!--Description-->
                    <div class="field {{if .Err_Description}}error{{end}}">
                        <label>{{.i18n.Tr "admin.users.description"}}</label>
                        <textarea name="description"
                                id="description"
                                placeholder="{{.i18n.Tr "admin.users.description"}}">{{.User.Description}}</textarea>
                    </div>
                    <!--Description-->

                    <!--Status / Admin-->
                    <div class="inline required field">
                        <div class="ui right toggle checkbox">
                            <input name="is_admin" type="checkbox" tabindex="0" class="hidden" {{if .User.IsAdmin}}checked="checked"{{end}}>
                            <label>{{.i18n.Tr "admin.users.is_admin"}}</label>
                        </div>

                        <div class="ui toggle checkbox">
                            <input name="is_active" type="checkbox" tabindex="0" class="hidden" {{if .User.IsActive}}checked="checked"{{end}}>
                            <label>{{.i18n.Tr "admin.users.is_active"}}</label>
                        </div>
                    </div>
                    <!--Status / Admin-->

                    <button class="ui button green" type="submit">{{.i18n.Tr "admin.users.update_user"}}</button>

                </form>

            </div>
        </div>
    </div>
</div>

{{template "base/footer" .}}