package cmd

import (
	"crypto/tls"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/fcgi"
	"path"
	"strings"

	"github.com/codegangsta/cli"
	"gopkg.in/macaron.v1"

	"github.com/go-macaron/binding"
	"github.com/go-macaron/cache"
	"github.com/go-macaron/captcha"
	"github.com/go-macaron/csrf"
	"github.com/go-macaron/gzip"
	"github.com/go-macaron/i18n"
	"github.com/go-macaron/session"
	"github.com/go-macaron/toolbox"

	"github.com/rlopes-fixeads/redLine/models"
	"github.com/rlopes-fixeads/redLine/modules/bindata"
	"github.com/rlopes-fixeads/redLine/modules/context"
	"github.com/rlopes-fixeads/redLine/modules/log"
	"github.com/rlopes-fixeads/redLine/modules/setting"
	"github.com/rlopes-fixeads/redLine/modules/template"
	"github.com/rlopes-fixeads/redLine/modules/validate"
	"github.com/rlopes-fixeads/redLine/routers"
	"github.com/rlopes-fixeads/redLine/routers/admin"
	"github.com/rlopes-fixeads/redLine/routers/dev"
	"github.com/rlopes-fixeads/redLine/routers/user"

	apiv1 "github.com/rlopes-fixeads/redLine/routers/api/v1"
	apiv1User "github.com/rlopes-fixeads/redLine/routers/api/v1/user"
)

type VerChecker struct {
	ImportPath string
	Version    func() string
	Expected   string
}

// checkVersion checks if binary matches the version of templates files.
func checkVersion() {
	// Templates.
	data, err := ioutil.ReadFile(setting.StaticRootPath + "/templates/.VERSION")
	if err != nil {
		log.Fatal(4, "Fail to read 'templates/.VERSION': %v", err)
	}

	if string(data) != setting.AppVer {
		log.Fatal(4, "Binary and template file version does not match, did you forget to recompile?")
	}
}

var CmdWeb = cli.Command{
	Name:        "web",
	Usage:       "Start RedLine web server",
	Description: `Redline is website of go.`,
	Action:      runWeb,
	Flags: []cli.Flag{
		stringFlag("port, p", "3000", "Temporary port number to prevent conflict"),
		stringFlag("config, c", "custom/conf/app.ini", "Custom configuration file path"),
	},
}

func newMacaron() *macaron.Macaron {
	m := macaron.New()
	// Logs
	if !setting.DisableRouterLog {
		m.Use(macaron.Logger())
	}

	// Gzip compress htmls
	m.Use(macaron.Recovery())
	if setting.EnableGzip {
		m.Use(gzip.Gziper())
	}

	// Protocol
	if setting.Protocol == setting.FCGI {
		m.SetURLPrefix(setting.AppSubUrl)
	}

	// Static folder
	m.Use(macaron.Static(
		path.Join(setting.StaticRootPath, "public"),
		macaron.StaticOptions{
			SkipLogging: setting.DisableRouterLog,
		},
	))

	// Upload avatar
	m.Use(macaron.Static(
		setting.AvatarUploadPath,
		macaron.StaticOptions{
			Prefix:      setting.AvatarUploadPrefix,
			SkipLogging: setting.DisableRouterLog,
		},
	))

	// template configurations
	m.Use(macaron.Renderer(macaron.RenderOptions{
		Directory:         path.Join(setting.StaticRootPath, "templates"),
		AppendDirectories: []string{path.Join(setting.CustomPath, "templates")},
		Funcs:             template.NewFuncMap(),
		IndentJSON:        macaron.Env != macaron.PROD,
	}))

	// read locale available
	localeNames, err := bindata.AssetDir("conf/locale")
	if err != nil {
		log.Fatal(4, "Fail to list locale files: %v", err)
	}
	localFiles := make(map[string][]byte)
	for _, name := range localeNames {
		localFiles[name] = bindata.MustAsset("conf/locale/" + name)
	}

	// config i18n
	m.Use(i18n.I18n(i18n.Options{
		SubURL:          setting.AppSubUrl,
		Files:           localFiles,
		CustomDirectory: path.Join(setting.CustomPath, "conf/locale"),
		Langs:           setting.Langs,
		Names:           setting.Names,
		DefaultLang:     "en-US",
		Redirect:        true,
	}))

	// Config cache
	m.Use(cache.Cacher(cache.Options{
		Adapter:       setting.CacheAdapter,
		AdapterConfig: setting.CacheConn,
		Interval:      setting.CacheInternal,
	}))

	m.Use(captcha.Captchaer(captcha.Options{
		SubURL: setting.AppSubUrl,
	}))

	m.Use(session.Sessioner(setting.SessionConfig))

	m.Use(csrf.Csrfer(csrf.Options{
		Secret:     setting.SecretKey,
		Cookie:     setting.CSRFCookieName,
		SetCookie:  true,
		Header:     "X-Csrf-Token",
		CookiePath: setting.AppSubUrl,
	}))

	// Database
	m.Use(toolbox.Toolboxer(m, toolbox.Options{
		HealthCheckFuncs: []*toolbox.HealthCheckFuncDesc{
			&toolbox.HealthCheckFuncDesc{
				Desc: "Database connection",
				Func: models.Ping,
			},
		},
	}))

	m.Use(context.Contexter())
	return m
}
func runWeb(ctx *cli.Context) {
	if ctx.IsSet("config") {
		setting.CustomConf = ctx.String("config")
	}
	routers.GlobalInit()
	checkVersion()

	m := newMacaron()

	reqSignIn := context.Toggle(&context.ToggleOptions{SignInRequired: true})
	//ignSignIn := context.Toggle(&context.ToggleOptions{SignInRequired: setting.Service.RequireSignInView})
	//ignSignInAndCsrf := context.Toggle(&context.ToggleOptions{DisableCSRF: true})
	reqSignOut := context.Toggle(&context.ToggleOptions{SignOutRequired: true})

	bindIgnErr := binding.BindIgnErr

	// ***** START: User public *****
	m.Get("/", routers.Home)
	m.Group("/user", func() {
		m.Get("/forget_password", user.ForgotPasswd)
		m.Post("/forget_password", user.ForgotPasswdPost)
		m.Get("/logout", user.SignOut)
	})
	// ***** END: User public *****

	// ***** START: User logout *****
	m.Group("/user", func() {
		m.Get("/sign_in", user.SignIn)
		m.Post("/sign_in", bindIgnErr(validate.SignInForm{}), user.SignInPost)
		m.Get("/sign_up", user.SignUp)
		m.Post("/sign_up", bindIgnErr(validate.RegisterForm{}), user.SignUpPost)
		m.Get("/reset_password", user.ResetPasswd)
		m.Post("/reset_password", user.ResetPasswdPost)
	}, reqSignOut)
	// ***** END: User logout *****

	// ***** START: User sign in *****
	m.Group("/user", func() {
		// Tickets Manager
		m.Group("/tickets", func() {
			m.Get("", user.ExploreTickets)
			m.Get("/:ticketid/view", user.ViewTicket)
			m.Combo("/new").Get(user.NewTicket).Post(bindIgnErr(validate.UserCreateTicketForm{}), user.NewTicketPost)
			m.Combo("/:ticketid").Get(user.EditTicket).Post(bindIgnErr(validate.UserEditTicketForm{}), user.EditTicketPost)
			m.Post("/delete", user.DeleteTicketPost)
		})

		m.Group("/tickets", func() {
			m.Get("/:ticketid/info", apiv1User.GetInfoTicketById)
		}, context.APIContexter())

		m.Group("/tickets", func() {
			m.Post("/:ticketid/info", bindIgnErr(validate.UserConfirmTicketProject{}), user.PostConfirmTicketById)
		})

	}, reqSignIn)
	// ***** END: User sign in *****

	// ***** START: User Auth ****/
	m.Group("/user/settings", func() {
		m.Get("", user.Settings)
		m.Post("", bindIgnErr(validate.UpdateProfileForm{}), user.SettingsPost)

		m.Get("/password", user.SettingsPassword)
		m.Post("/password", bindIgnErr(validate.ChangePasswordForm{}), user.SettingsPasswordPost)

	}, reqSignIn)
	// ***** START: User Auth ****/

	reqSignAdmin := context.Toggle(&context.ToggleOptions{SignInRequired: true, AdminRequired: true})

	/**** START ADMIN ****/
	m.Group("/admin", func() {
		// Home Admin
		m.Get("", reqSignAdmin, admin.Dashboard)

		// Country Manager
		m.Group("/countries", func() {
			m.Get("", admin.ExploreCountries)

			m.Combo("/new").Get(admin.NewCountry).Post(bindIgnErr(validate.AdminCreateCountryForm{}), admin.NewCountryPost)
			m.Combo("/:countryid").Get(admin.EditCountry).Post(bindIgnErr(validate.AdminEditCountryForm{}), admin.EditCountryPost)
			m.Post("/delete", admin.DeleteCountryPost)
		})

		// Users Manager
		m.Group("/users", func() {
			m.Get("", admin.ExploreUsers)

			// Users Projects Manager
			m.Group("/:userid/projects", func() {
				m.Get("", admin.UsersProjectsGet)
				m.Post("", bindIgnErr(validate.AdminUserAssociateProjectForm{}), admin.UsersProjectsPost)
				m.Post("/delete", admin.DeleteUsersProjectsPost)
			})

			m.Combo("/new").Get(admin.NewUser).Post(bindIgnErr(validate.AdminCreateUserForm{}), admin.NewUserPost)
			m.Combo("/:userid").Get(admin.EditUser).Post(bindIgnErr(validate.AdminEditUserForm{}), admin.EditUserPost)

			m.Post("/delete", admin.DeleteUserPost)
		})

		// Tickets Manager
		m.Group("/tickets", func() {
			m.Get("", admin.ExploreTickets)
			m.Get("/:ticketid/view", admin.ViewTicket)

			m.Combo("/new").Get(admin.NewTicket).Post(bindIgnErr(validate.AdminCreateTicketForm{}), admin.NewTicketPost)
			m.Combo("/:ticketid").Get(admin.EditTicket).Post(bindIgnErr(validate.AdminEditTicketForm{}), admin.EditTicketPost)
			m.Post("/delete", admin.DeleteTicketPost)
		})

		// Categories Manager
		m.Group("/categories", func() {
			m.Get("", admin.ExploreCategories)

			m.Combo("/new").Get(admin.NewCategory).Post(bindIgnErr(validate.AdminCreateCategoryForm{}), admin.NewCategoryPost)
			m.Combo("/:categoryid").Get(admin.EditCategory).Post(bindIgnErr(validate.AdminEditCategoryForm{}), admin.EditCategoryPost)
			m.Post("/delete", admin.DeleteCategoryPost)
		})

		// Projects Manager
		m.Group("/projects", func() {
			m.Get("", admin.ExploreProjects)
			m.Get("/:projectid/users", admin.UsersFromProjectGet)

			m.Combo("/new").Get(admin.NewProject).Post(bindIgnErr(validate.AdminCreateProjectForm{}), admin.NewProjectPost)
			m.Combo("/:projectid").Get(admin.EditProject).Post(bindIgnErr(validate.AdminEditProjectForm{}), admin.EditProjectPost)
			m.Post("/delete", admin.DeleteProjectPost)
		})

	}, reqSignAdmin)
	/**** END ADMIN ****/

	if macaron.Env == macaron.DEV {
		m.Get("/template/*", dev.TemplatePreview)
	}

	m.Group("/api", func() {
		apiv1.RegisterRoutes(m)
	})

	// Not found handler.
	m.NotFound(routers.NotFound)

	// Flag for port number in case first time run conflict.
	if ctx.IsSet("port") {
		setting.AppUrl = strings.Replace(setting.AppUrl, setting.HttpPort, ctx.String("port"), 1)
		setting.HttpPort = ctx.String("port")
	}

	var err error
	listenAddr := fmt.Sprintf("%s:%s", setting.HttpAddr, setting.HttpPort)
	log.Info("Listen: %v://%s%s", setting.Protocol, listenAddr, setting.AppSubUrl)
	switch setting.Protocol {
	case setting.HTTP:
		err = http.ListenAndServe(listenAddr, m)
	case setting.HTTPS:
		server := &http.Server{Addr: listenAddr, TLSConfig: &tls.Config{MinVersion: tls.VersionTLS10}, Handler: m}
		err = server.ListenAndServeTLS(setting.CertFile, setting.KeyFile)
	case setting.FCGI:
		err = fcgi.Serve(nil, m)
	default:
		log.Fatal(4, "Invalid protocol: %s", setting.Protocol)
	}

	if err != nil {
		log.Fatal(4, "Fail to start server: %v", err)
	}
}
