var i;

// create a closure
(function () {
    'use strict';

    // csrf it for security of website.
    var csrf = $('meta[name=_csrf]').attr("content");

    // when jquery already loaded call it.
    $(function () {
        $.ajaxSetup({
            beforeSend: function (xhr) {
                xhr.setRequestHeader("_csrf", csrf);
            }
        });

        $('.ui.dropdown').dropdown();
        $('.ui.checkbox').checkbox();
        $('[data-content]').popup();

        $('.new-ticket-button').click(showModalNewTicket);
        $('.delete-button').click(confirmDeleteAction);
        $('.confirm-implemented').click(showModalConfirmProjectImplemented);

        if (window.hasOwnProperty('hljs')) {
            $('pre code').each(function (i, block) {
                hljs.initHighlighting(block);
            });
        }

        editorMarkDown();
        initEmojify();
    });

    // open modal confirm ticket has been implemented in project
    function showModalConfirmProjectImplemented() {
        var self = $(this);
        var url  = self.data('url');

        $.ajax(url).done(openModal);

        function openModal(data) {
            var resource     = !!data.resource && data.resource;
            var modelOptions = {
                closable: false,
                onShow: onShow,
                onHidden: onHidden
            };

            var idProject = null;
            try {
                if (!!window.location.hash) {
                    idProject = window.location.hash.slice(1);
                }
            } catch (e) {
            }

            var template = [];
            for (var i in resource.projects) {
                var project = !!resource.projects[i] && resource.projects[i] || {};


                console.log(project.name, project.status)
                switch (project.status) {
                    case 2:
                        project.status = 'checked="checked"';
                        break;
                    default:
                        project.status = '';
                        break;
                }

                var classTr = '';
                if (!!idProject && idProject == project.id) {
                    classTr = 'class="positive"';
                }

                var tmp = '' +
                    '<tr ' + classTr + '>' +
                    '    <td>' + project.name + '</td>' +
                    '    <td class="collapsing">' +
                    '        <div class="ui fitted slider checkbox">' +
                    '            <input class="confirm_project" type="checkbox" name="project_uid" value="' + project.id + '" ' + project.status + '> <label></label>' +
                    '        </div>' +
                    '    </td>' +
                    '</tr>';

                template.push(tmp);
            }

            function onShow() {
                $('#modal_project_confirm_title').html(resource.name);
                $('#modal_project_confirm_body').html(template.join(''));
                $('#model_form_project_confirm').attr('action', url);
                $('#ticket_id').val(resource.id);
            }

            function onHidden() {
                $('#modal_project_confirm_title').html('');
                $('#modal_project_confirm_body').html('');
                $('#model_form_project_confirm').removeAttr('action');
                $('#ticket_id').val('');
            }

            $('.ui.project.confirm.modal')
                .modal(modelOptions)
                .modal('show');

        }
    }

    // open modal create new ticket
    function showModalNewTicket() {
        $('.ui.new.ticket.modal').modal('show');
    }

    // open modal confirm delete.
    function confirmDeleteAction() {
        var self = $(this);
        var url  = self.data('url');

        var modelOptions = {
            closable: false,
            onApprove: onApprove
        };

        $('.delete.modal')
            .modal(modelOptions)
            .modal('show');

        function onApprove() {
            if (self.data('type') == "form") {
                $(self.data('form')).submit();
                return;
            }

            var postData = {
                '_csrf': csrf,
                'id': self.data('id')
            };

            $.post(url, postData).done(reload);
        }

        return false;
    }

    // reload page
    function reload(data) {
        window.location.href = !!data.redirect && data.redirect || window.location.pathname;
    }

    function editorMarkDown() {
        if ($('.markdown-editor#edit-area').length == 0) {
            return;
        }

        if ($('.markdown-editor#edit-area').length > 0) {
            var $edit_area = $('#edit-area');
            var simplemde  = new SimpleMDE({
                autoDownloadFontAwesome: false,
                element: $edit_area[0],
                previewRender: function (plainText, preview) { // Async method
                    setTimeout(function () {
                        var postData = {
                            "_csrf"  : csrf,
                            "mode"   : "gfm",
                            "context": $edit_area.data('description'),
                            "text"   : plainText
                        };

                        var request = $.ajax({
                            url     : $edit_area.data('url'),
                            method  : "POST",
                            data    : postData,
                            dataType: "html"
                        });

                        request.done(function (data) {
                            preview.innerHTML = '<div class="markdown">' + data + '</div>';
                            emojify.run($('.editor-preview')[0]);
                        });

                        request.fail(function (jqXHR, textStatus) {
                            console.error("Request failed: " + textStatus);
                        });
                    }, 0);
                    return "Loading...";
                },
                renderingConfig: {
                    singleLineBreaks: false
                },
                spellChecker: false,
                tabSize: 4,
                toolbar: ["bold", "italic", "strikethrough", "|",
                    "heading", "heading-1", "heading-2", "heading-3", "|",
                    "code", "quote", "|",
                    "unordered-list", "ordered-list", "|",
                    "link", "image", "horizontal-rule", "|",
                    "preview", "fullscreen"]
            })
        }


    }

    function initEmojify() {
        // Emojify
        emojify.setConfig({
            img_dir: '/assets/emojify.js/dist/images/basic/',
            ignore_emoticons: true
        });
        var hasEmoji = document.getElementsByClassName('emojify');
        for (var i = 0; i < hasEmoji.length; i++) {
            emojify.run(hasEmoji[i]);
        }
    }
})();