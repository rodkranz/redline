package main

import (
	"os"
	"runtime"

	"github.com/codegangsta/cli"

	"github.com/rlopes-fixeads/redLine/cmd"
	"github.com/rlopes-fixeads/redLine/modules/setting"
)

const APP_VER = "1.0.0"

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	setting.AppVer = APP_VER
}

func main() {
	app := cli.NewApp()
	app.Name = "RedLine"
	app.Usage = "RedLine init service"
	app.Version = APP_VER
	app.Commands = []cli.Command{
		cmd.CmdWeb,
	}

	app.Flags = append(app.Flags, []cli.Flag{}...)
	app.Run(os.Args)
}
